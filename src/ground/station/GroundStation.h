/*
 * GroundStation.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include <omnetpp.h>
#include <optional>
#include <string>

#include "inet/common/Simsignals.h"
#include "inet/networklayer/ipv4/Ipv4Header_m.h"
#include "inet/transportlayer/tcp_common/TcpHeader_m.h"

#include "core/communication/dynamicgate/DynamicGate.h"
#include "core/communication/dynamicgate/DynamicGateSupport.h"
#include "core/mobility/ground/GroundStationMobility.h"
#include "core/interfaces/IPositionAware.h"
#include "core/mobility/libnorad/cEci.h"
#include "core/mobility/libnorad/cJulian.h"
#include "core/mobility/libnorad/cSite.h"
#include "core/mobility/libnorad/ccoord.h"
#include "orbit/satellite/base/Satellite.h"
#include "networklayer/common/CstlRoutingTable.h"

using namespace omnetpp;

namespace florasat {

class GroundStation : public cSimpleModule, public core::IPositionAware, public DynamicGateSupport {
   public:
    DynamicGate* getDynamicGatesByIndex(DynamicGateType gate_type) const override;

    double getElevation(const cJulian* time, const Satellite* other) const;

    /** Returns the latitude of this entity in radians. */
    double getLatitude() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getLatitude();
    }

    /** Returns the latitude of this entity in radians. */
    double getLongitude() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getLongitude();
    }

    /** Returns the altitude of this entity in meters. */
    double getAltitude() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getAltitude();
    }

    /** Returns the ECEF position of this entity. */
    const cEcef* getPosition() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getPosition();
    }

   protected:
    ~GroundStation();
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    virtual void handleMessage(cMessage* msg) override;

    void set_name(std::string groundstation_name);

   protected:
   private:
    // cached_ptrs
    GroundStationMobility* groundstation_mobility = nullptr;
    cGate* ground_packet_handler_queue_in = nullptr;
    DynamicGate* dynamic_gsls = nullptr;
    cCoordGeo* coord = nullptr;
};

}  // namespace florasat
