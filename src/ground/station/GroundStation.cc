/*
 * GroundStation.cc
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#include "GroundStation.h"

namespace florasat {

Define_Module(GroundStation);

GroundStation::~GroundStation() {
    delete coord;
}

void GroundStation::initialize(int stage) {
    if (stage == inet::INITSTAGE_LOCAL) {
        groundstation_mobility = check_and_cast<GroundStationMobility*>(getSubmodule("groundstation_mobility"));
        dynamic_gsls = check_and_cast<DynamicGate*>(getSubmodule("satellite_link"));
        ground_packet_handler_queue_in = getSubmodule("ground_packet_handler")->gate("queueIn");
    } else if (stage == inet::INITSTAGE_PHYSICAL_ENVIRONMENT) {
        double latRad = deg2rad(par("latitude_deg"));
        double lonRad = deg2rad(par("longitude_deg"));
        double altMeters = par("altitude_km").doubleValue() * 1000.0;
        groundstation_mobility->initialize_position(latRad, lonRad, altMeters);

        coord = new cCoordGeo(latRad, lonRad, altMeters);

        std::string groundstation_name = par("groundstation_name").stdstringValue();
        set_name(groundstation_name);
    }
}

void GroundStation::handleMessage(cMessage* msg) {
    sendDirect(msg, ground_packet_handler_queue_in);
}

void GroundStation::set_name(std::string groundstation_name) {
    auto& displayString = getDisplayString();
    displayString.setTagArg("t", 0, groundstation_name.c_str());
    displayString.setTagArg("t", 2, "black");
}

DynamicGate* GroundStation::getDynamicGatesByIndex(DynamicGateType gate_type) const {
    if (gate_type == DynamicGateType::GSL) {
        return dynamic_gsls;
    } else {
        throw cRuntimeError("Unexpected gates_index %u. Expecting 0=GSL.", gate_type);
    }
}

double GroundStation::getElevation(const cJulian* time, const Satellite* satellite) const {
    auto eci_gs = cEci(*coord, *time);

    auto eci_sat = cEci();
    double sat_span_minutes = satellite->getOrbit()->TPlusEpoch(*time) / 60.0;
    satellite->getOrbit()->getPosition(sat_span_minutes, eci_sat);
    eci_sat.ae2km();

    cVector connectingVector(eci_gs.getPos().m_x - eci_sat.getPos().m_x, eci_gs.getPos().m_y - eci_sat.getPos().m_y, eci_gs.getPos().m_z - eci_sat.getPos().m_z);
    return connectingVector.Angle(eci_gs.getPos()) - HALFPI;
};

}  // namespace florasat
