/*
 * GroundStationRouting.h
 *
 *  Created on: May 19, 2023
 *      Author: Robin Ohs
 */

#ifndef __FLORA_GROUND_GROUNDSTATIONROUTINGBASE_H_
#define __FLORA_GROUND_GROUNDSTATIONROUTINGBASE_H_

#include <omnetpp.h>

#include <set>
#include <sstream>

#include "core/Constants.h"
#include "core/interfaces/IPositionAware.h"
#include "core/utils/SetUtils.h"
#include "core/mobility/ground/GroundStationMobility.h"

using namespace omnetpp;
using namespace florasat::core;

namespace florasat {
namespace ground {

class GroundStationRoutingBase : public cSimpleModule, public IPositionAware {
   public:
    int getGroundStationId() const { return groundStationId; }
    cGate *getInputGate(int index);
    cGate *getOutputGate(int index);

    const std::set<int> &getSatellites() const;
    void removeSatellite(int satId);
    void addSatellite(int satId);
    bool isConnectedTo(int satId);
    bool isConnectedToAnySat();

    /** Returns the latitude of this entity in radians. */
    double getLatitude() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getLatitude();
    }

    /** Returns the latitude of this entity in radians. */
    double getLongitude() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getLongitude();
    }

    /** Returns the altitude of this entity in meters. */
    double getAltitude() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getAltitude();
    }

    /** Returns the ECEF position of this entity. */
    const cEcef *getPosition() const override {
        ASSERT(groundstation_mobility != nullptr);
        return groundstation_mobility->getPosition();
    }

    friend std::ostream &operator<<(std::ostream &ss, const GroundStationRoutingBase &gs) {
        ss << "{";
        ss << "\"groundStationId\": " << gs.groundStationId << ",";
        ss << "\"satellites\": "
           << "[";
        for (int satellite : gs.satellites) {
            ss << satellite << ",";
        }
        ss << "],";
        ss << "}";
        return ss;
    }

   protected:
    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }

   private:
    int groundStationId;
    std::set<int> satellites;
    GroundStationMobility *groundstation_mobility;
};

}  // namespace ground
}  // namespace florasat

#endif  // __FLORA_GROUND_GROUNDSTATIONROUTINGBASE_H_
