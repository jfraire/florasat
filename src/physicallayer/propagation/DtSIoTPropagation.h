/*
 * DtSIoTPropagation.h
 *
 *  Created on: May 15, 2022
 *      Author: diego
 */

#ifndef LORAPHY_PROPAGATION_DTSIOTPROPAGATION_H_
#define LORAPHY_PROPAGATION_DTSIOTPROPAGATION_H_

#include <math.h>

#include "inet/physicallayer/wireless/common/base/packetlevel/PropagationBase.h"
#include "inet/physicallayer/wireless/common/signal/Arrival.h"

#include "../unitdisk/SatelliteUnitDiskTransmission.h"
#include "LoRa/base/radio/LoRaRadio.h"
#include "LoRa/LoRaPhy/LoRaTransmission.h"
#include "core/mobility/ground/GroundStationMobility.h"
#include "core/mobility/libnorad/cEcef.h"
#include "core/mobility/libnorad/cEci.h"
#include "core/mobility/libnorad/ccoord.h"
#include "core/mobility/ground/UniformGroundMobility.h"
#include "core/mobility/satellite/SatelliteMobility.h"

namespace florasat {

using namespace physicallayer;

/**Class: DtSIoTPropagation
 * Within this model the distance between two positions are calculated using the coordinates of the source and destination.
 * This distanced is used to calculate the propagation delay for nodes within a satellite constellation.
 * Written by Aiden Valentine
 * Modified by Diego Maldonado
 */
class DtSIoTPropagation : public PropagationBase {
   protected:
    bool ignoreMovementDuringTransmission;
    bool ignoreMovementDuringPropagation;
    bool ignoreMovementDuringReception;

   protected:
    virtual void initialize(int stage) override;
    virtual const Coord computeArrivalPosition(const simtime_t startTime, const Coord startPosition, IMobility *mobility) const;

   public:
    DtSIoTPropagation();

    virtual std::ostream &printToStream(std::ostream &stream, int level, int evFlags = 0) const override;
    virtual const IArrival *computeArrival(const ITransmission *transmission, IMobility *mobility) const override;
};

}  // namespace florasat

#endif /* LORAPHY_PROPAGATION_DTSIOTPROPAGATION_H_ */
