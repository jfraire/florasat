/*
 * MeasuringChannel.h
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#include "MeasuringChannel.h"

namespace florasat {

Register_Class(MeasuringChannel);

MeasuringChannel::MeasuringChannel(const char* name) : cDatarateChannel(name) {}

MeasuringChannel::~MeasuringChannel() {}

void MeasuringChannel::initialize() {
    cDatarateChannel::initialize();
}

cChannel::Result MeasuringChannel::processMessage(cMessage* msg, const SendOptions& options, simtime_t t) {
    cChannel::Result result = cDatarateChannel::processMessage(msg, options, t);

    if (auto packet = dynamic_cast<inet::Packet*>(msg)) {
        // transmission time
        simtime_t transmission_time = packet->getBitLength() / getDatarate();
        simtime_t bit_transmission_time = 1 / getDatarate();
        // EV << "Packet (" << packet->getBitLength() << ") transmitted with DR(" << getDatarate() << ") takes " << transmission_time.inUnit(SimTimeUnit::SIMTIME_NS) << " transmission time. Time per bit (" << bit_transmission_time.inUnit(SimTimeUnit::SIMTIME_NS) << ")" << endl;
        inet::increaseTimeTag<TransmissionTimeTag>(packet, bit_transmission_time, transmission_time);

        // propagation delay
        simtime_t propagation_time = getDelay();
        simtime_t bit_propagation_time = propagation_time / packet->getBitLength();
        // EV << "Packet (" << packet->getBitLength() << ") will propagate for " << propagation_time.inUnit(SimTimeUnit::SIMTIME_US) << ". Each bit: " << bit_propagation_time << endl;
        inet::increaseTimeTag<PropagationTimeTag>(packet, bit_propagation_time, propagation_time);
    }
    return result;
}

}  // namespace florasat
