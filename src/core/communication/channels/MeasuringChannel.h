/*
 * MeasuringChannel.h
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include "inet/common/TimeTag.h"
#include "inet/common/packet/Packet_m.h"

using namespace omnetpp;
using namespace inet;

namespace florasat {

//
// This channels adds support for flow measurements like transmission and propagation delay on a per packet base.
//
class MeasuringChannel : public cDatarateChannel {
   public:
    explicit MeasuringChannel(const char *name = nullptr);
    virtual ~MeasuringChannel();

    virtual void initialize() override;

    /**
     * Stores transmission and propagation delay inside packet TimeTags for flows.
     */
    virtual cChannel::Result processMessage(cMessage *msg, const SendOptions &options, simtime_t t) override;
};

}  // namespace florasat
