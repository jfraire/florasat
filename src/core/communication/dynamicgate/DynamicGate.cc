/*
 * DynamicGate.cc
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#include "DynamicGate.h"

namespace florasat {

Define_Module(DynamicGate);

DynamicGate::~DynamicGate() {
    for (auto gateState : gateStates) {
        delete gateState;
    }
    gateStates.clear();
}

void DynamicGate::initialize(int stage) {
    cSimpleModule::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        subjectModule = inet::findModuleFromPar<cModule>(par("subjectModule"), this);
        if (subjectModule == nullptr) {
            error("Could not access subject module from DynamicGate.");
        }

        limit = par("limit").intValue();

        // create submodule vector for interfaces at subject
        gate_vector_name = par("name").stdstringValue() + std::string("-dynamic-gates");
        subjectModule->addSubmoduleVector(gate_vector_name.c_str(), 0);
    }
}

std::optional<GateState*> DynamicGate::findGateTo(size_t satId) const {
    for (size_t i = 0; i < gateStates.size(); i++) {
        auto gateState = gateStates[i];
        if (gateState->partner == satId) {
            return {gateState};
        }
    }
    return std::nullopt;
}

std::vector<size_t> DynamicGate::getCommunicationPartners() const {
    std::vector<size_t> partners;
    for (size_t i = 0; i < gateStates.size(); i++) {
        auto gateState = gateStates[i];
        if (gateState->partner.has_value()) {
            partners.push_back(gateState->partner.value());
        }
    }
    return partners;
}

GateState* DynamicGate::findFreeOrCreateGate() {
    for (size_t i = 0; i < gateStates.size(); i++) {
        auto gateState = gateStates[i];
        if (gateState->partner == std::nullopt) {
            return gateState;
        }
    }
    size_t new_gate_id = createNewGate();
    return gateStates[new_gate_id];
}

void DynamicGate::markGateConnected(size_t gateId, size_t partnerId) {
    ASSERT(gateId >= 0 && gateId < gateStates.size());
    auto& gateState = gateStates[gateId];
    ASSERT(!gateState->partner.has_value() || gateState->partner == partnerId);
    gateState->partner = partnerId;
}

void DynamicGate::markGateDisconnected(size_t gateId) {
    ASSERT(gateId >= 0 && gateId < gateStates.size());
    auto& gateState = gateStates[gateId];
    ASSERT(gateState->partner.has_value());
    gateState->partner = std::nullopt;
}

size_t DynamicGate::createNewGate() {
    ASSERT(subjectModule != nullptr);
    // check if submodule vector for holding interfaces exist
    int tmp_size = subjectModule->getSubmoduleVectorSize(gate_vector_name.c_str());

    if (tmp_size >= limit && limit != -1) {
        throw cRuntimeError("DynamicGate module (%s) is full: %d/%d gates exists.", gate_vector_name.c_str(), tmp_size, limit);
    }

    subjectModule->setSubmoduleVectorSize(gate_vector_name.c_str(), tmp_size + 1);

    size_t next_id = gateStates.size();
    // external gates
    std::ostringstream gate_name_ext_stream;
    gate_name_ext_stream << gate_vector_name << "GATE-EXT-" << next_id;
    auto gate_name_ext = gate_name_ext_stream.str();
    subjectModule->addGate(gate_name_ext.c_str(), cGate::Type::INOUT);
    cGate* inputExt = subjectModule->gateHalf(gate_name_ext.c_str(), cGate::INPUT);
    cGate* outputExt = subjectModule->gateHalf(gate_name_ext.c_str(), cGate::OUTPUT);
    // internal gate
    // std::ostringstream gate_name_int_stream;
    // gate_name_int_stream << "GSL-INT-" << next_id;
    // auto gate_name_int = gate_name_int_stream.str();
    // cGate* outputInt = subjectModule->add(gate_name_int.c_str(), cGate::Type::OUTPUT);

    // create interface module
    cModule* queue_interface = omnetpp::cModuleType::get("florasat.core.communication.dynamicgate.QueueInterface")->create(gate_vector_name.c_str(), subjectModule, tmp_size);
    // set parameters
    queue_interface->par("switchingTime").setDoubleValue(par("switchingTime").doubleValue());
    queue_interface->par("packetCapacity").setIntValue(par("packetCapacity").intValue());
    queue_interface->callInitialize();

    auto& displayString = queue_interface->getDisplayString();
    displayString.setTagArg("p", 0, next_width);
    displayString.setTagArg("p", 1, 875 + par("y_offset").intValue());
    next_width += 150;

    cGate* ifIn = queue_interface->gate("in");
    cGate* ifOut = queue_interface->gate("out");

    ifOut->connectTo(outputExt);

    GateState* new_gsl_state = new GateState();
    new_gsl_state->id = next_id;
    new_gsl_state->input = inputExt;
    new_gsl_state->toIfQueue = ifIn;
    new_gsl_state->outerOutput = outputExt;
    new_gsl_state->partner = std::nullopt;
    gateStates.emplace_back(new_gsl_state);
    return next_id;
}

}  // namespace florasat
