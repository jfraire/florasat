/*
 * MeasuringChannel.h
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#include "Interface.h"

namespace florasat {

Define_Module(Interface);

void Interface::initialize(int stage) {
    ClockUserModuleMixin::initialize(stage);
    if (stage == inet::INITSTAGE_LOCAL) {
        collectionTimer = new ClockEvent("CollectionTimer");
        ifOutGate = gate("ifOut");
    }
}

void Interface::handleMessage(cMessage *msg) {
    if (msg == collectionTimer) {
        EV_DEBUG << simTime().dbl() << ": Collect packet due to timer." << endl;
        collectPacket();
    }
}

void Interface::collectPacket() {
    Enter_Method("collectPacket");

    cGate *gate = inputGate->getPathStartGate();
    if (!provider->canPullSomePacket(gate)) {
        EV_DEBUG << simTime().dbl() << ": Cannot pull another packet" << endl;
        return;
    }

    cChannel *channel = ifOutGate->findTransmissionChannel();
    if (channel == nullptr) {
        Packet *pkt = provider->pullPacket(gate);
        take(pkt);

        EV_DEBUG << simTime().dbl() << ": Not connected, delete pkt" << pkt->getId() << endl;
        handlePacketProcessed(pkt);

        // drop packet
        PacketDropDetails details;
        details.setReason(PacketDropReason::INTERFACE_DOWN);
        emit(packetDroppedSignal, pkt, &details);
        numDroppedPackets++;
        delete pkt;

        // continue with next packets
        collectPacket();
    } else {
        if (channel->isBusy()) {
            if (!collectionTimer->isScheduled()) {
                EV_DEBUG << simTime().dbl() << ": Channel busy, schedule new event at " << channel->getTransmissionFinishTime().dbl() << endl;
                cancelClockEvent(collectionTimer);
                scheduleAt(channel->getTransmissionFinishTime(), collectionTimer);
            } else {
                EV_DEBUG << simTime().dbl() << ": Channel busy, but event already scheduled for " << collectionTimer->getArrivalTime().dbl() << endl;
            }
        } else {
            Packet *pkt = provider->pullPacket(gate);
            take(pkt);
            EV_DEBUG << simTime().dbl() << ": Send packet " << pkt->getId() << endl;
            handlePacketProcessed(pkt);
            send(pkt, "ifOut");
            collectPacket();
        }
    }
    updateDisplayString();
}

void Interface::handleCanPullPacketChanged(cGate *gate) {
    Enter_Method("handleCanPullPacketChanged");
    // if there is currently no packet in "processing", schedule the processing of the next package
    EV_DEBUG << simTime().dbl() << ": Can pull packet" << endl;
    collectPacket();
}

void Interface::handlePullPacketProcessed(Packet *packet, cGate *gate, bool successful) {
    Enter_Method("handlePullPacketProcessed");
}

std::string Interface::resolveDirective(char directive) const {
    static std::string result;
    switch (directive) {
        case 'p':
            result = std::to_string(numProcessedPackets);
            break;
        case 'd':
            result = std::to_string(numDroppedPackets);
            break;
        case 'l':
            result = processedTotalLength.str();
            break;
        default:
            throw cRuntimeError("Unknown directive: %c", directive);
            break;
    }
    return result.c_str();
}

}  // namespace florasat
