/*
 * DynamicGate.cc
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>
#include <optional>

#include "GateState.h"

#include "inet/common/INETDefs_m.h"
#include "inet/common/ModuleAccess.h"

namespace florasat {

using namespace omnetpp;
using namespace inet;

class DynamicGate : public cSimpleModule {
   public:
    std::optional<GateState*> findGateTo(size_t partnerId) const;
    GateState* findFreeOrCreateGate();
    std::vector<size_t> getCommunicationPartners() const;
    void markGateConnected(size_t gateId, size_t partnerId);
    void markGateDisconnected(size_t gateId);

   protected:
    ~DynamicGate();
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    size_t createNewGate();

   protected:
    /** @brief Pointer to visual representation module, to speed up repeated access. */

    // cached ptrs
    cModule* subjectModule = nullptr;

    // parameters
    int limit = -1;
    std::string gate_vector_name;

    // state
    std::vector<GateState*> gateStates;
    int next_width = 275;
};

}  // namespace florasat
