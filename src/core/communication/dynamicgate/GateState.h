/*
 * GateState.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include <omnetpp.h>
#include <optional>

namespace florasat {

using namespace omnetpp;

struct GateState {
    size_t id;  // id of this struct in vector
    cGate* input = nullptr;
    cGate* toIfQueue = nullptr;
    cGate* outerOutput = nullptr;
    std::optional<size_t> partner = std::nullopt;
};

}  // namespace florasat