/*
 * DynamicGate.h
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include <optional>

#include "DynamicGate.h"
#include "core/communication/channels/MeasuringChannel.h"

namespace florasat {

using namespace omnetpp;

enum class DynamicGateType { GSL,
                             ISL };

class DynamicGateSupport {
   public:
    virtual ~DynamicGateSupport() {}

    virtual DynamicGate* getDynamicGatesByIndex(DynamicGateType gate_type) const = 0;

    virtual cGate* getNextInputGate(DynamicGateType gate_type, size_t partnerId);
    virtual void connectToDyn(DynamicGateType gate_type, size_t partnerId, cGate* partnerInput, double delay, inet::units::values::bps datarate);
    virtual void disconnectDyn(DynamicGateType gate_type, size_t partnerId);
    virtual std::optional<cGate*> getGateToPartner(DynamicGateType gate_type, size_t partnerId) const;
    virtual std::vector<size_t> getPartners(DynamicGateType gate_type) const;
};

}  // namespace florasat
