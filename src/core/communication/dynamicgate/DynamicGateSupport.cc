/*
 * DynamicGate.cc
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#include "DynamicGateSupport.h"

namespace florasat {

using namespace omnetpp;

cGate* DynamicGateSupport::getNextInputGate(DynamicGateType gate_type, size_t partnerId) {
    DynamicGate* gates = getDynamicGatesByIndex(gate_type);
    ASSERT(gates != nullptr);
    auto gateState_opt = gates->findGateTo(partnerId);
    if (gateState_opt.has_value()) {
        return gateState_opt.value()->input;
    } else {
        GateState* free_gsl = gates->findFreeOrCreateGate();
        return free_gsl->input;
    }
}

void DynamicGateSupport::connectToDyn(DynamicGateType gate_type, size_t partnerId, cGate* partnerInput, double delay, inet::units::values::bps datarate) {
    DynamicGate* gates = getDynamicGatesByIndex(gate_type);
    ASSERT(gates != nullptr);
    auto gate_state_opt = gates->findGateTo(partnerId);

    // connect gates
    if (gate_state_opt.has_value()) {
        auto gate_state = gate_state_opt.value();
        MeasuringChannel* channel_0 = check_and_cast<MeasuringChannel*>(gate_state->outerOutput->getChannel());
        channel_0->setDelay(delay);
    } else {
        GateState* free_gsl = gates->findFreeOrCreateGate();
        gates->markGateConnected(free_gsl->id, partnerId);
        MeasuringChannel* channel_0 = dynamic_cast<MeasuringChannel*>(cChannelType::find("florasat.core.communication.channels.MeasuringChannel")->create("GSL"));
        channel_0->setDelay(delay);
        channel_0->setDatarate(datarate.get());
        free_gsl->outerOutput->connectTo(partnerInput, channel_0);
        channel_0->callInitialize();
    }
}

void DynamicGateSupport::disconnectDyn(DynamicGateType gate_type, size_t partnerId) {
    DynamicGate* gates = getDynamicGatesByIndex(gate_type);
    ASSERT(gates != nullptr);

    auto gate_state_opt = gates->findGateTo(partnerId);
    if (!gate_state_opt.has_value()) return;  // connection does not exist
    auto gate_state = gate_state_opt.value();
    gates->markGateDisconnected(gate_state->id);
    gate_state->outerOutput->disconnect();
}

std::optional<cGate*> DynamicGateSupport::getGateToPartner(DynamicGateType gate_type, size_t partnerId) const {
    DynamicGate* gates = getDynamicGatesByIndex(gate_type);
    ASSERT(gates != nullptr);

    auto gateState_opt = gates->findGateTo(partnerId);
    if (gateState_opt.has_value()) {
        return {gateState_opt.value()->toIfQueue};
    } else {
        return std::nullopt;
    }
}

std::vector<size_t> DynamicGateSupport::getPartners(DynamicGateType gate_type) const {
    DynamicGate* gates = getDynamicGatesByIndex(gate_type);
    ASSERT(gates != nullptr);
    return gates->getCommunicationPartners();
};

}  // namespace florasat
