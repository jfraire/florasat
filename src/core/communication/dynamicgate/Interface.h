/*
 * MeasuringChannel.h
 *
 * Created on: Jan 04, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include "inet/common/INETDefs_m.h"
#include "inet/common/Simsignals_m.h"
#include "inet/common/clock/ClockUserModuleMixin.h"
#include "inet/queueing/base/ActivePacketSinkBase.h"

using namespace omnetpp;
using namespace inet;
using namespace inet::queueing;

namespace florasat {

class Interface : public ClockUserModuleMixin<ActivePacketSinkBase> {
   public:
    virtual void handleCanPullPacketChanged(cGate *gate) override;
    virtual void handlePullPacketProcessed(Packet *packet, cGate *gate, bool successful) override;

    std::string resolveDirective(char directive) const override;

   protected:
    ~Interface() { cancelAndDeleteClockEvent(collectionTimer); }

    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void handleMessage(cMessage *msg) override;

    // packet sink
    void collectPacket();

   protected:
    // cached pointers
    cGate *ifOutGate = nullptr;
    ClockEvent *collectionTimer = nullptr;

    size_t numDroppedPackets = 0;
};

}  // namespace florasat
