/*
 * SatelliteMobility.h
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>
#include <string>

#include "inet/common/INETDefs.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/geometry/common/CanvasProjection.h"
#include "inet/mobility/base/MobilityBase.h"

#include "core/common/Timer.h"
#include "core/mobility/libnorad/cEcef.h"
#include "core/mobility/libnorad/cEci.h"
#include "core/mobility/libnorad/cOrbitA.h"

namespace florasat {

using namespace omnetpp;
using namespace inet;

class SatelliteMobility : public cSimpleModule {
   public:
    void updatePosition(double lat, double lon, double alt, bool ascending);

    void setOrbit(cOrbitA* orbit) {
        ASSERT(orbit != nullptr);
        this->orbit = orbit;
    }

    const cOrbitA* getOrbit() const {
        ASSERT(this->orbit != nullptr);
        return this->orbit;
    }

    /** Returns the ecef position of this entity. */
    const cEcef* getPosition() const {
        ASSERT(last_position != nullptr);
        return last_position;
    }

    /** Returns the ecef position of this entity at the given simtime. */
    cEcef getPositionAt(simtime_t time) const {
        auto orbit = getOrbit();
        double span_in_min = time.dbl() / 60;
        cEci eci = cEci();
        orbit->getPosition(span_in_min, eci);
        auto coord = eci.toGeo();

        // longitude
        double lonRad = coord.m_LonRad;
        // fix west not negative
        if (lonRad > PI) {
            lonRad -= TWOPI;
        }
        ASSERT(lonRad >= -TWOPI || lonRad <= TWOPI);

        // latitude
        double latRad = coord.m_LatRad;
        ASSERT(latRad >= -PI || latRad <= PI);
        // double lat = rad2deg(latRad);

        // altitude in meters
        double altMeters = coord.m_AltMeters;

        return cEcef(latRad, lonRad, altMeters);
    }

    /** Returns if the satellite is ascending.
     * Satellite is ascending if Argument of Latitude (AOL) is in [0.0°, 90°] or [270.0, 360.0°).
     */
    bool isAscending() const {
        return is_ascending;
    }
    /** Returns the latitude of this entity. */
    double getLatitude() const {
        return last_lat;
    }
    /** Returns the latitude of this entity. */
    double getLongitude() const {
        return last_lon;
    }
    /** Returns the latitude of this entity. */
    double getAltitude() const {
        return last_alt;
    }

    double getDistance(const double& refLatitude, const double& refLongitude, const double& refAltitude) const {
        cEcef ecefDestCoord = cEcef(refLatitude, refLongitude, refAltitude);
        return last_position->getDistance(&ecefDestCoord);
    }

   protected:
    ~SatelliteMobility();
    int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    void initialize(int stage) override;

    void update_data(double lat, double lon, double alt, bool ascending);
    void update_ui();

   protected:
    /** @brief Pointer to visual representation module, to speed up repeated access. */
    cModule* subjectModule;
    /** @brief The 2D projection used on the canvas. */
    const CanvasProjection* canvasProjection;

    cOrbitA* orbit = nullptr;

    double last_lat = 45.0;
    double last_lon = 45.0;
    double last_alt = 600.0;
    bool is_ascending = false;
    Coord position = Coord::ZERO;
    cEcef* last_position = nullptr;

    int mapX;
    int mapY;
};

}  // namespace florasat
