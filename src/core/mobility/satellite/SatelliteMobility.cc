/*
 * SatelliteMobility.cc
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#include "SatelliteMobility.h"

namespace florasat {

Define_Module(SatelliteMobility);

SatelliteMobility::~SatelliteMobility() {
    delete last_position;
}

void SatelliteMobility::initialize(int stage) {
    cSimpleModule::initialize(stage);
    // EV << "Satellite Initialize " << stage << endl;
    if (stage == inet::INITSTAGE_LOCAL) {
        subjectModule = inet::findModuleFromPar<cModule>(par("subjectModule"), this);
        if (subjectModule == nullptr) {
            error("Could not access satellite for satellite mobility.");
        }
        canvasProjection = CanvasProjection::getCanvasProjection(subjectModule->getCanvas());

        last_position = new cEcef();

        mapX = std::atoi(getSystemModule()->getDisplayString().getTagArg("bgb", 0));
        mapY = std::atoi(getSystemModule()->getDisplayString().getTagArg("bgb", 1));
    }
}

void SatelliteMobility::updatePosition(double latRad, double lonRad, double alt, bool ascending) {
    update_data(latRad, lonRad, alt, ascending);
    update_ui();
}

void SatelliteMobility::update_data(double latRad, double lonRad, double alt, bool ascending) {
    is_ascending = ascending;
    last_lat = latRad;
    last_lon = lonRad;
    last_alt = alt;
    delete last_position;
    // EV << "lla=" << latRad << "," << lonRad << "," << alt << endl;
    last_position = new cEcef(latRad, lonRad, alt);
    position.x = ((mapX * lonRad) / (2 * PI)) + (mapX / 2);  // x canvas position, longitude projection
    position.y = ((-mapY * latRad) / PI) + (mapY / 2);       // y canvas position, latitude projection
    position.z = alt;                                        // real satellite altitude in meters
}

void SatelliteMobility::update_ui() {
    if (getEnvir()->isGUI()) {
        ASSERT(subjectModule != nullptr);
        ASSERT(canvasProjection != nullptr);

        auto subjectModulePosition = canvasProjection->computeCanvasPoint(position);
        char buf[32];
        snprintf(buf, sizeof(buf), "%lf", subjectModulePosition.x);
        buf[sizeof(buf) - 1] = 0;
        auto& displayString = subjectModule->getDisplayString();
        displayString.setTagArg("p", 0, buf);
        snprintf(buf, sizeof(buf), "%lf", subjectModulePosition.y);
        buf[sizeof(buf) - 1] = 0;
        displayString.setTagArg("p", 1, buf);
    }
}

}  // namespace florasat