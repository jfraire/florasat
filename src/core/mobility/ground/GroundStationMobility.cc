/*
 * GroundStationMobility.cc
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#include "GroundStationMobility.h"

namespace florasat {

Define_Module(GroundStationMobility);

GroundStationMobility::~GroundStationMobility() {
    delete m_position;
}

void GroundStationMobility::initialize(int stage) {
    cSimpleModule::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        subjectModule = inet::findModuleFromPar<cModule>(par("subjectModule"), this);
        if (subjectModule == nullptr) {
            error("Could not access ground station from groundstation mobility.");
        }
        canvasProjection = CanvasProjection::getCanvasProjection(subjectModule->getCanvas());

        mapX = std::atoi(getSystemModule()->getDisplayString().getTagArg("bgb", 0));
        mapY = std::atoi(getSystemModule()->getDisplayString().getTagArg("bgb", 1));
    }
}

void GroundStationMobility::initialize_position(double latRad, double lonRad, double altMeters) {
    ASSERT(subjectModule != nullptr);
    ASSERT(canvasProjection != nullptr);

    VALIDATE(latRad >= -HALFPI && latRad <= HALFPI);
    VALIDATE(lonRad >= -PI && lonRad <= PI);

    m_latRad = latRad;
    m_lonRad = lonRad;
    m_altMeters = altMeters;

    m_position = new cEcef(latRad, lonRad, altMeters);

    Coord tmp_position = Coord();
    tmp_position.x = ((mapX * m_lonRad) / TWOPI) + (mapX / 2);  // x canvas position, longitude projection
    tmp_position.y = ((-mapY * m_latRad) / PI) + (mapY / 2);    // y canvas position, latitude projection
    tmp_position.z = altMeters;

    auto subjectModulePosition = canvasProjection->computeCanvasPoint(tmp_position);
    char buf[32];
    snprintf(buf, sizeof(buf), "%lf", subjectModulePosition.x);
    buf[sizeof(buf) - 1] = 0;
    auto& displayString = subjectModule->getDisplayString();
    displayString.setTagArg("p", 0, buf);
    snprintf(buf, sizeof(buf), "%lf", subjectModulePosition.y);
    buf[sizeof(buf) - 1] = 0;
    displayString.setTagArg("p", 1, buf);
}

}  // namespace florasat
