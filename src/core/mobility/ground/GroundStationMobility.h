/*
 * GroundStationMobility.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include "inet/mobility/static/StationaryMobility.h"

#include "core/macros/Validate.h"
#include "core/mobility/libnorad/cEcef.h"
#include "core/mobility/libnorad/cEci.h"
#include "core/mobility/libnorad/cSite.h"

namespace florasat {

using namespace omnetpp;
using namespace inet;

class GroundStationMobility : public cSimpleModule {
   public:
    double getLatitude() const {
        return m_latRad;
    };
    double getLongitude() const {
        return m_lonRad;
    };
    double getAltitude() const {
        return m_altMeters;
    }

    cEcef* getPosition() const {
        ASSERT(m_position != nullptr);
        return m_position;
    }

    double getDistance(const double& refLatitude, const double& refLongitude, const double& refAltitude) const {
        cEcef ecefDestCoord = cEcef(refLatitude, refLongitude, refAltitude);
        return m_position->getDistance(&ecefDestCoord);
    }

    void initialize_position(double latRad, double lonRad, double altMeters);

   protected:
    ~GroundStationMobility();
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

   protected:
    /** @brief Pointer to visual representation module, to speed up repeated access. */
    cModule* subjectModule;
    /** @brief The 2D projection used on the canvas. */
    const CanvasProjection* canvasProjection;

    double m_latRad, m_lonRad, m_altMeters;

    cEcef* m_position = nullptr;

    int mapX;
    int mapY;
};

}  // namespace florasat
