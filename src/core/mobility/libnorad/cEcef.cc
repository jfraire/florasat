#include "cEcef.h"

#include <math.h>

#include "ccoord.h"
#include "globals.h"
//////////////////////////////////////////////////////////////////////
// cEcef(cCoordGeo&)
// Calculate the ECEF coordinates of a location, for simple distance calculations.
// Measurement is in meters unlike OS3 being in kilometers, therefore the altitude is translated.
// Reference: https://www2.unb.ca/gge/Pubs/LN39.pdf (GEODETIC POSITIONS COMPUTATIONS by E. J. KRAKIWSKY and D. B. THOMSON)
// Written by Aiden Valentine
cEcef::cEcef() {
    m_x = 0.0;
    m_y = 0.0;
    m_z = 0.0;
    m_latRad = 0.0;
    m_lonRad = 0.0;
    m_altMeters = 0.0;
}

/**
 * Constructor which, upon being given longitude,latitude and altitude coordinates, calculates the ECEF coordinates.
 */
cEcef::cEcef(cCoordGeo geoCoordinates) {
    update(geoCoordinates.m_LatRad, geoCoordinates.m_LonRad, geoCoordinates.m_AltMeters);
}

cEcef::cEcef(double xx, double yy, double zz, int n) {
    m_x = xx;
    m_y = yy;
    m_z = zz;

    double p = sqrt(sqr(m_x) + sqr(m_y));
    double theta = atan2(m_z * kR, p * kB);

    m_latRad = atan2(m_z + kEd2 * kB * pow(sin(theta), 3), p - e2 * kR * pow(cos(theta), 3));
    m_lonRad = atan2(m_y, m_x);
    m_altMeters = (p / cos(m_latRad)) - (kR / sqrt(1.0 - e2 * pow(sin(m_latRad), 2)));
}

cEcef::cEcef(double latRad, double lonRad, double altMeters) {
    update(latRad, lonRad, altMeters);
}

/**
 * Get Distances between two ECEF coordinates using Pythagoras' Theorem
 */
double cEcef::getDistance(const cEcef* receiverEcef) const {
    double x2 = receiverEcef->getX();
    double y2 = receiverEcef->getY();
    double z2 = receiverEcef->getZ();
    return sqrt(sqr(x2 - m_x) + sqr(y2 - m_y) + sqr(z2 - m_z));
}

void cEcef::update(double latRad, double lonRad, double altMeters) {
    m_latRad = latRad;
    m_lonRad = lonRad;
    m_altMeters = altMeters;

    double NPhi = SEMI_MAJOR_AXIS / sqrt(1 - e2 * sqr(sin(m_latRad)));

    m_x = (NPhi + m_altMeters) * cos(m_latRad) * cos(m_lonRad);
    m_y = (NPhi + m_altMeters) * cos(m_latRad) * sin(m_lonRad);
    m_z = (NPhi * (1 - e2) + m_altMeters) * sin(m_latRad);
}
