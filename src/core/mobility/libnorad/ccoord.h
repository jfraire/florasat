//-----------------------------------------------------
// coord.h
//
// Copyright 2002-2003 Michael F. Henry
//-----------------------------------------------------
#ifndef __LIBNORAD_ccoord_H__
#define __LIBNORAD_ccoord_H__

#include "globals.h"

//-----------------------------------------------------
// Geocentric coordinates.
//-----------------------------------------------------
class cCoordGeo {
   public:
    cCoordGeo();
    cCoordGeo(double latRad, double lonRad, double altMeters) : m_LatRad(latRad), m_LonRad(lonRad), m_AltMeters(altMeters) {}
    virtual ~cCoordGeo(){};

    double m_LatRad;  // Latitude,  radians (negative south)
    double m_LonRad;  // Longitude, radians (negative west)
    double m_AltMeters;  // Altitude,  meters      (above mean sea level)
};

//-----------------------------------------------------
// Topocentric-Horizon coordinates.
//-----------------------------------------------------
class cCoordTopo {
   public:
    cCoordTopo();
    cCoordTopo(double az, double el, double rng, double rate) : m_Az(az), m_El(el), m_Range(rng), m_RangeRate(rate) {}
    virtual ~cCoordTopo(){};

    double m_Az;         // Azimuth, radians
    double m_El;         // Elevation, radians
    double m_Range;      // Range, kilometers
    double m_RangeRate;  // Range rate of change, km/sec
                         // Negative value means "towards observer"
};

#endif
