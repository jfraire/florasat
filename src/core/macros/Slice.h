/*
 * Slice.h
 *
 * Taken from: https://stackoverflow.com/questions/50549611/slicing-a-vector-in-c
 * Version: 1.0
 */

#pragma once

namespace florasat {

template <int left = 0, int right = 0, typename T>
constexpr auto slice(T&& container) {
    if constexpr (right > 0) {
        return std::span(begin(std::forward<T>(container)) + left, begin(std::forward<T>(container)) + right);
    } else {
        return std::span(begin(std::forward<T>(container)) + left, end(std::forward<T>(container)) + right);
    }
}

}  // namespace florasat
