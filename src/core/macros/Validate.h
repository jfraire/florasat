/*
 * Validate.h
 *
 *  Created on: Jan 05, 2024
 *      Author: Robin Ohs
 */

#pragma once

namespace florasat {

/**
 * @brief Macros that can be used to validate expressions (even in production).
 * If expr evaluates to false, an exception will be thrown with
 * file/line/function information.
 */
#define VALIDATE(expr)                                                          \
    ((void)((expr) ? 0                                                          \
                   : (throw omnetpp::cRuntimeError(                             \
                          "VALIDATE: Condition '%s' does not hold in function " \
                          "'%s' at %s:%d",                                      \
                          #expr, __FUNCTION__, __FILE__, __LINE__),             \
                      0)))

/**
 * @brief Macros that can be used to validate expressions (even in production).
 * If expr evaluates to false, an exception will be thrown with
 * file/line/function information and the given text.
 */
#define VALIDATE2(expr, text)                                             \
    ((void)((expr) ? 0                                                    \
                   : (throw omnetpp::cRuntimeError(                       \
                          "VALIDATE: %s in function '%s' at %s:%d", text, \
                          __FUNCTION__, __FILE__, __LINE__),              \
                      0)))

}  // namespace florasat
