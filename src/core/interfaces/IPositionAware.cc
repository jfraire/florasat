/*
 * IPositionAware.h
 *
 *  Created on: Jan 05, 2024
 *      Author: Robin Ohs
 */

#include "IPositionAware.h"

namespace florasat {
namespace core {

/** Returns the distance in meters from this entity to a reference entity. */
double IPositionAware::getDistance(const IPositionAware* other) const {
    ASSERT(other != nullptr);
    ASSERT(other->getPosition() != nullptr);
    ASSERT(getPosition() != nullptr);
    return getPosition()->getDistance(other->getPosition());
}

}  // namespace core
}  // namespace florasat