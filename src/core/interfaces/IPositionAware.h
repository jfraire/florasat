/*
 * IPositionAware.h
 *
 *  Created on: Jan 05, 2024
 *      Author: Robin Ohs
 */

#pragma once

#include <iostream>
#include <omnetpp.h>

#include "core/mobility/libnorad/cEcef.h"

namespace florasat {
namespace core {

class IPositionAware {
   public:
    /** Returns the latitude of this entity. */
    virtual double getLatitude() const = 0;
    /** Returns the longitude of this entity. */
    virtual double getLongitude() const = 0;
    /** Returns the altitude of this entity. */
    virtual double getAltitude() const = 0;

    virtual const cEcef* getPosition() const = 0;

    /** Returns the distance in meters from this entity to a reference entity. */
    double getDistance(const IPositionAware* other) const;
};

}  // namespace core
}  // namespace florasat