/*
 * Timer.h
 *
 *  Created on: Jan 05, 2024
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>
#include <chrono>

namespace florasat {
namespace core {

inline double round_to(double value, int decimals = 0) {
    ASSERT(decimals >= 0);
    int multiplicator = std::pow(10, decimals);
    return std::round(value * multiplicator) / multiplicator;
}

}  // namespace core
}  // namespace florasat