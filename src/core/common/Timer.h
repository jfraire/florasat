/*
 * Timer.h
 *
 *  Created on: Apr 06, 2023
 *  Updated on: Jan 05, 2024
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>
#include <chrono>

using namespace std::chrono;

namespace florasat {
namespace core {

class Timer {
   public:
    Timer() {
        reset();
    }

    template <typename T> float getTime() {
        time_point<high_resolution_clock> now = high_resolution_clock::now();
        return duration_cast<T>(now - start).count();
    }

    omnetpp::simtime_t getElapsedSimTime() {
        int64_t procTime = (int64_t)round(getTime<microseconds>());
        return omnetpp::SimTime(procTime, omnetpp::SimTimeUnit::SIMTIME_US);
    }

    void reset() {
        start = high_resolution_clock::now();
    }

   private:
    time_point<high_resolution_clock> start;
};

}  // namespace core
}  // namespace florasat
