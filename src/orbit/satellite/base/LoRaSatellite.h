/*
 * LoRaSatellite.h
 *
 *  Created on: May 7, 2024
 *      Author: d
 */

#ifndef ORBIT_SATELLITE_BASE_LORASATELLITE_H_
#define ORBIT_SATELLITE_BASE_LORASATELLITE_H_

#pragma once

#include "Satellite.h"

using namespace omnetpp;

namespace florasat {

class LoRaSatellite : Satellite
{

};

}


#endif /* ORBIT_SATELLITE_BASE_LORASATELLITE_H_ */
