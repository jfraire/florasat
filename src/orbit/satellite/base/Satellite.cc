/*
 * Satellite.cc
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#include "Satellite.h"

namespace florasat {

Define_Module(Satellite);

Satellite::~Satellite() {
}

void Satellite::initialize(int stage) {
    if (stage == inet::INITSTAGE_LOCAL) {
        satellite_mobility = check_and_cast<SatelliteMobility*>(getSubmodule("satellite_mobility"));

        // connections
        dynamic_gsls = check_and_cast<DynamicGate*>(getSubmodule("groundstation_link"));
        dynamic_isls = check_and_cast<DynamicGate*>(getSubmodule("intersatellite_link"));
        packet_handler = check_and_cast<IPacketHandler*>(getSubmodule("packet_handler"));
    }
}

void Satellite::handleMessage(cMessage* msg) {
    EV << "Sat received msg " << msg << endl;
    if (auto packet = dynamic_cast<inet::Packet*>(msg)) {
        packet_handler->handlePacket(packet);
    } else
        throw cRuntimeError("unknown message format arrived on gate '%s'", msg->getArrivalGate()->getName());
}

DynamicGate* Satellite::getDynamicGatesByIndex(DynamicGateType gate_type) const {
    if (gate_type == DynamicGateType::GSL) {
        return dynamic_gsls;
    } else if (gate_type == DynamicGateType::ISL) {
        return dynamic_isls;
    } else {
        throw cRuntimeError("Unexpected gates_index %u. Expecting 0=GSL or 1=ISL.", gate_type);
    }
}

}  // namespace florasat
