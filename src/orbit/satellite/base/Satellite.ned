// Satellite.ned
// Author: Robin Ohs
// Version: 1.0

package florasat.orbit.satellite.base;

import florasat.core.mobility.satellite.SatelliteMobility;
import florasat.core.communication.dynamicgate.DynamicGate;
import florasat.networklayer.packethandlers.ISatellitePacketHandler;

module Satellite {
    parameters:
        @networkNode;
        @class(Satellite);
        @display("i=satellit_blue");

        double switchingTime @unit(s);
        int packetCapacity;

        @figure[networkLayer](type=rectangle; pos=250,308; size=1000,134; fillColor=#00ff00; lineColor=#808080; cornerRadius=5; fillOpacity=0.1);
        @figure[networkLayer.title](type=text; pos=1245,313; anchor=ne; text="network layer");
        @figure[linkLayer](type=rectangle; pos=250,458; size=1000,284; fillColor=#0000ff; lineColor=#808080; cornerRadius=5; fillOpacity=0.1);
        @figure[linkLayer.title](type=text; pos=1245,463; anchor=ne; text="link layer");
        @figure[interfaceLayer](type=rectangle; pos=250,758; size=1000,360; fillColor=#00ffff; lineColor=#808080; cornerRadius=5; fillOpacity=0.1);
        @figure[interfaceLayer.title](type=text; pos=1245,763; anchor=ne; text="interface layer");

    submodules:
        satellite_mobility: SatelliteMobility {
            @display("p=102,310");
        }
        intersatellite_link: DynamicGate {
            @display("p=102,510");
            name = "ISL";
            limit = 4;
            switchingTime = parent.switchingTime;
            packetCapacity = parent.packetCapacity;
        }
        groundstation_link: DynamicGate {
            @display("p=102,410");
            name = "GSL";
            switchingTime = parent.switchingTime;
            packetCapacity = parent.packetCapacity;
            y_offset = 50;
        }

        packet_handler: <default("")> like ISatellitePacketHandler {
            @display("p=375,375;q=queue");
        }

        // routing: <> like IRoutingBase;

}