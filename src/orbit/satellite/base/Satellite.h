/*
 * Satellite.h
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>
#include <optional>
#include <string>

#include "inet/common/INETDefs.h"


#include "core/communication/dynamicgate/DynamicGate.h"
#include "core/communication/dynamicgate/DynamicGateSupport.h"
#include "core/mobility/satellite/SatelliteMobility.h"
#include "core/interfaces/IPositionAware.h"
#include "core/macros/Validate.h"
#include "core/mobility/libnorad/cEcef.h"
#include "networklayer/packethandlers/IPacketHandler.h"

using namespace omnetpp;

namespace florasat {

class Satellite : public cSimpleModule, public core::IPositionAware, public DynamicGateSupport {
   public:
    // dynamic ISLs and GSLs
    DynamicGate* getDynamicGatesByIndex(DynamicGateType gate_type) const override;

    /** Returns if the satellite is ascending.
     * Satellite is ascending if Argument of Latitude (AOL) is in [0.0°, 90°] or [270.0, 360.0°).
     */
    bool isAscending() const {
        ASSERT(satellite_mobility != nullptr);
        return satellite_mobility->isAscending();
    }

    /** Returns the latitude of this entity. */
    double getLatitude() const override {
        ASSERT(satellite_mobility != nullptr);
        return satellite_mobility->getLatitude();
    }

    double getLongitude() const override {
        ASSERT(satellite_mobility != nullptr);
        return satellite_mobility->getLongitude();
    }

    double getAltitude() const override {
        ASSERT(satellite_mobility != nullptr);
        return satellite_mobility->getAltitude();
    }

    /** Returns the ECEF position of this entity. */
    const cEcef* getPosition() const override {
        ASSERT(satellite_mobility != nullptr);
        return satellite_mobility->getPosition();
    }

    /** Returns the ECEF position of this entity at a given time. */
    cEcef getPositionAt(simtime_t time) const {
        ASSERT(satellite_mobility != nullptr);
        return satellite_mobility->getPositionAt(time);
    }

    const cOrbitA* getOrbit() const {
        ASSERT(satellite_mobility != nullptr);
        return satellite_mobility->getOrbit();
    }

   protected:
    ~Satellite();
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    virtual void handleMessage(cMessage* msg) override;

   private:
    // cached_ptrs
    SatelliteMobility* satellite_mobility = nullptr;
    DynamicGate* dynamic_gsls = nullptr;
    DynamicGate* dynamic_isls = nullptr;
    IPacketHandler* packet_handler = nullptr;
};

}  // namespace florasat
