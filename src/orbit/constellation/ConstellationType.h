/*
 * ConstellationType.h
 *
 * Created on: Jan 08, 2024
 *     Author: Robin Ohs
 */

#pragma once

namespace florasat {

enum class ConstellationType {
    WALKER_STAR,
    WALKER_DELTA,
};

} // florasat
