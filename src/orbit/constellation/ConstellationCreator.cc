/*
 * ConstellationCreator.cc
 *
 * Created on: Jan 21, 2023
 * Updated on: Jan 05, 2024
 *     Author: Robin Ohs
 */

#include "ConstellationCreator.h"

namespace florasat {

Define_Module(ConstellationCreator);

void ConstellationCreator::initialize(int stage) {
    // EV << "ConstellationCreator Initialize " << stage << endl;
    if (stage == inet::INITSTAGE_LOCAL) {
        // cache ptrs
        simulation_module = getSystemModule();
        std::string constellation_type = par("constellation_type");
        satelliteType = par("satelliteType");

        inclination = par("inclination");
        plane_count = par("plane_count");
        sats_per_plane = par("sats_per_plane");
        inter_plane_spacing = par("inter_plane_spacing");
        altitude_meter = par("altitude_km").doubleValue() * 1000.0;

        bstar = par("bstar");
        drag = par("drag");

        base_year = par("base_year");
        base_day = par("base_day");

        eccentricity = 0.000001;

        // set raan_spread
        if (constellation_type == "Walker-Delta") {
            raan_spread = 360.0;
        } else if (constellation_type == "Walker-Star") {
            raan_spread = 180.0;
        } else {
            error("Unexpected constellation type '%s'.", constellation_type.c_str());
        }

        sat_count = sats_per_plane * plane_count;

        // validation
        VALIDATE2(simulation_module != nullptr, "Simulation module was not found");
        VALIDATE2(inclination > 0 && inclination < 180,
                  "Inclination must be in (0,180)");
        VALIDATE2(sat_count > 0, "Number of satellites must be > 0");
        VALIDATE2(plane_count > 0, "Plane_count must be > 0");
        VALIDATE2(inter_plane_spacing >= 0 && inter_plane_spacing < plane_count,
                  "Inter_plane_spacing must be in [0,plane_count)");
        VALIDATE2(altitude_meter > 0, "Altitude must be > 0");
        create_constellation();
    }
}

void ConstellationCreator::create_constellation() {
    double raan_delta = raan_spread / plane_count;     // ΔΩ = 2𝜋/𝑃 in [0,2𝜋]
    double phase_difference = 360.0 / sats_per_plane;  // ΔΦ = 2𝜋/Q in [0,2𝜋]
    double phase_offset =
        (360.0 * inter_plane_spacing) / sat_count;  // Δ𝑓 = 2𝜋𝐹/𝑃𝑄 in [0,2𝜋)
    // VALIDATE(raan_delta >= 0 && raan_delta <= 360.0);
    // VALIDATE(phase_difference >= 0 && phase_difference <= 360.0);
    // VALIDATE(phase_offset >= 0 && phase_offset < 360.0);

    // create submodule vector
    simulation_module->addSubmoduleVector("satellites", sat_count);

    // iterate over planes
    for (size_t plane_index = 0; plane_index < plane_count; plane_index++) {
        double raan = raan_delta * plane_index;
        // VALIDATE(raan >= 0 && raan < 360);
        // iterate over satellites in plane
        for (size_t sat_in_plane_index = 0; sat_in_plane_index < sats_per_plane;
             sat_in_plane_index++) {
            int index = sat_in_plane_index + plane_index * sats_per_plane;
            double mean_anomaly = std::fmod(
                plane_index * phase_offset + sat_in_plane_index * phase_difference,
                360.0);
            // VALIDATE(mean_anomaly >= 0.0 && mean_anomaly < 360.0);
            configure_satellite(index, raan, mean_anomaly, plane_index);
        }
    }
}

void ConstellationCreator::configure_satellite(int index, double raan, double mean_anomaly, int plane_index) {
    cModule* satellite = omnetpp::cModuleType::get(satelliteType)->create("satellites", simulation_module, index);
    satellite->callInitialize(0);

    auto sat_mobility = check_and_cast<SatelliteMobility*>(satellite->getSubmodule("satellite_mobility"));

    // std::string name = std::format("sat-{}", index);
    std::string name = std::string("test");

    cOrbitA* orbit = new cOrbitA(name, base_year, base_day, altitude_meter, eccentricity, inclination, mean_anomaly, bstar, drag, index, plane_count, sats_per_plane, raan, 0);

    sat_mobility->setOrbit(orbit);
}

}  // namespace florasat
