/*
 * TopologyControlBase.cc
 *
 * Created on: May 05, 2023
 *     Author: Robin Ohs
 */

#include "TopologyControlBase.h"

namespace florasat {

Register_Abstract_Class(TopologyControlBase);

TopologyControlBase::~TopologyControlBase() {
    delete m_time;
    cancelAndDelete(selfMsg);
}

void TopologyControlBase::initialize(int stage) {
    // EV << "TopologyControl Initialize " << stage << endl;
    ClockUserModuleMixin::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        // thread_pool = check_and_cast<core::ThreadPool*>(getSystemModule()->getSubmodule("thread_pool"));
        update_interval = par("update_interval");
        selfMsg = new ClockEvent("sendTimer");

        std::string raw_constellation_type = par("constellation_type");
        plane_count = par("plane_count");
        sats_per_plane = par("sats_per_plane");
        inter_plane_spacing = par("inter_plane_spacing");
        sat_count = sats_per_plane * plane_count;

        isl_datarate = bps(par("isl_datarate").intValue());
        gsl_datarate = bps(par("gsl_datarate").intValue());
        min_elevation_rad = deg2rad(par("min_elevation_deg"));

        // parse constellation type
        if (raw_constellation_type == "Walker-Star") {
            constellation_type = ConstellationType::WALKER_STAR;
        } else if (raw_constellation_type == "Walker-Delta") {
            constellation_type = ConstellationType::WALKER_DELTA;
        } else {
            error("Unexpected constellation type '%s'.", raw_constellation_type.c_str());
        }

        // get orbit epoch, base and gap
        int base_year = par("base_year");
        double base_day = par("base_day");
        m_time = new cJulian(base_year, base_day);
    } else if (stage == inet::INITSTAGE_CLOCK) {
        scheduleClockEventAfter(update_interval, selfMsg);
    } else if (stage == inet::INITSTAGE_PHYSICAL_LAYER) {
        initialize_topology();
    } else if (stage == inet::INITSTAGE_LAST) {
        if (par("validate_continuous_connectivity").boolValue()) {
            validate_continuous_connectivity();
        }
    }
}

void TopologyControlBase::handleMessage(cMessage *msg) {
    if (!msg->isSelfMessage()) {
        error("TopologyControl only accepts self-messages");
    }
    rescheduleClockEventAfter(update_interval, selfMsg);
    m_time->addSec(update_interval.dbl());
    update_topology();
}

const RoutingTopology &TopologyControlBase::get_topology() const {
    return m_topology;
}

const RoutingTopology TopologyControlBase::get_topology_at(simtime_t time) const {
    RoutingTopology topo = create_new_unconnected_topo(time);

    // add inter-plane ISLs to topo
    update_intra_plane_isls(topo, false);
    update_inter_plane_isls(topo, false);
    update_groundstation_links(topo, false);

    return topo;
}

void TopologyControlBase::initialize_topology() {
    load_satellites();
    load_groundstations();

    m_topology = create_new_unconnected_topo(simTime());

    core::Timer timer = core::Timer();
    update_intra_plane_isls(m_topology, true);
    auto micro_seconds = timer.getTime<microseconds>();
    EV_DEBUG << "Setup Intra-Plane ISLs took " << micro_seconds / 1000.0 << "ms" << endl;

    timer.reset();
    update_inter_plane_isls(m_topology, true);
    micro_seconds = timer.getTime<microseconds>();
    EV_DEBUG << "Setup Inter-Plane ISLs took " << micro_seconds / 1000.0 << "ms" << endl;

    timer.reset();
    update_groundstation_links(m_topology, true);
    micro_seconds = timer.getTime<microseconds>();
    EV_DEBUG << "Setup GSLs took " << micro_seconds / 1000.0 << "ms" << endl;
}

void TopologyControlBase::update_topology() {
    core::Timer timer = core::Timer();
    load_satellite_positions();
    auto micro_seconds = timer.getTime<microseconds>();
    EV_DEBUG << "Load sat positions" << micro_seconds / 1000.0 << "ms" << endl;

    timer.reset();
    update_inter_plane_isls(m_topology, true);
    micro_seconds = timer.getTime<microseconds>();
    EV_DEBUG << "Update Inter-Plane ISLs took " << micro_seconds / 1000.0 << "ms" << endl;

    timer.reset();
    update_groundstation_links(m_topology, true);
    micro_seconds = timer.getTime<microseconds>();
    EV_DEBUG << "Update GSLs took " << micro_seconds / 1000.0 << "ms" << endl;

    // m_topology.print();
}

std::optional<double> TopologyControlBase::calculate_remaining_visibility(size_t gs_id, size_t sat_id) const {
    auto gs = getGroundStationForUpdate(gs_id);
    auto sat = getSatelliteForUpdate(sat_id);

    // check if they are connected
    if (!gs->getGateToPartner(DynamicGateType::GSL, sat_id).has_value()) {
        return std::nullopt;
    }

    auto time_tmp = *m_time;
    double step_length = update_interval.dbl();
    int steps = 1;
    while (true) {
        time_tmp.addSec(step_length);
        double elevation = gs->getElevation(&time_tmp, sat);
        if (elevation < min_elevation_rad) {
            EV_DEBUG << gs_id << "->" << sat_id << ": No longer connected after " << steps << " steps" << endl;
            break;
        }
        steps++;
    }
    return {steps * step_length};
}

RoutingTopology TopologyControlBase::create_new_unconnected_topo(simtime_t time) const {
    // init satellite nodes
    std::vector<Node> satellites;
    satellites.reserve(sat_count);
    for (size_t i = 0; i < sat_count; i++) {
        auto pos = getSatellite(i)->getPositionAt(time);
        satellites.emplace_back(Node(NodeType::SAT, cVector(pos.getX(), pos.getY(), pos.getZ())));
    }

    // init groundstation nodes
    std::vector<Node> groundstations;
    satellites.reserve(gs_count);
    for (size_t i = 0; i < gs_count; i++) {
        const cEcef *pos = getGroundStation(i)->getPosition();
        groundstations.emplace_back(Node(NodeType::GS, cVector(pos->getX(), pos->getY(), pos->getZ())));
    }

    return RoutingTopology(satellites, groundstations);
}

void TopologyControlBase::validate_continuous_connectivity() {
    int start = 0;
    auto end_raw = getEnvir()->getConfig()->getConfigValue("sim-time-limit");
    int end = 0;
    if (end_raw == nullptr) {
        end = 20000;
    } else {
        end = atoi(end_raw);
    }
    EV_DEBUG << "Start: " << start << " -> End: " << end << endl;

    double i = start;
    cJulian time_tmp = *m_time;
    while (i < end) {
        for (auto gs : m_groundstations) {
            bool found = false;
            for (auto sat : m_satellites) {
                double elevation = gs->getElevation(&time_tmp, sat);
                if (elevation >= min_elevation_rad) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                throw cRuntimeError("GS %d is not connected to any satellite at simtime %f", gs->getIndex(), i);
            }
        }

        i += update_interval.dbl();
        time_tmp.addSec(update_interval.dbl());
    }
}

void TopologyControlBase::load_satellites() {
    sat_count = getSystemModule()->getSubmoduleVectorSize("satellites");
    m_satellites.reserve(sat_count);

    for (size_t i = 0; i < sat_count; i++) {
        auto sat = check_and_cast<Satellite *>(getSystemModule()->getSubmodule("satellites", i));
        m_satellites.push_back(sat);
    }
}

void TopologyControlBase::load_groundstations() {
    gs_count = getSystemModule()->getSubmoduleVectorSize("groundstation");
    m_groundstations.reserve(gs_count);

    for (size_t i = 0; i < gs_count; i++) {
        auto gs = check_and_cast<GroundStation *>(getSystemModule()->getSubmodule("groundstation", i));
        m_groundstations.push_back(gs);
    }
}

void TopologyControlBase::load_satellite_positions() {
    for (auto test : m_satellites) {
        const cEcef *pos = test->getPosition();
        m_topology.updatePosition(test->getIndex(), pos->getX(), pos->getY(), pos->getZ());
    }
}

void TopologyControlBase::connect_satellites(RoutingTopology &topo, bool connect_modules, size_t first_sat_id, size_t second_sat_id) const {
    double distance = topo.calculateSatsDistance(first_sat_id, second_sat_id);
    topo.connectISL(first_sat_id, second_sat_id, distance);

    if (connect_modules) {
        auto first_module = getSatelliteForUpdate(first_sat_id);
        auto second_module = getSatelliteForUpdate(second_sat_id);

        double delay = distance / SPEED_OF_LIGHT;

        auto first_sat_input = first_module->getNextInputGate(DynamicGateType::ISL, second_sat_id);
        auto second_sat_input = second_module->getNextInputGate(DynamicGateType::ISL, first_sat_id);

        first_module->connectToDyn(DynamicGateType::ISL, second_sat_id, second_sat_input, delay, isl_datarate);
        second_module->connectToDyn(DynamicGateType::ISL, first_sat_id, first_sat_input, delay, isl_datarate);
    }
}

void TopologyControlBase::disconnect_satellites(RoutingTopology &topo, bool connect_modules, size_t first_sat_id, size_t second_sat_id) const {
    topo.disconnectISL(first_sat_id, second_sat_id);

    if (connect_modules) {
        auto first_module = getSatelliteForUpdate(first_sat_id);
        auto second_module = getSatelliteForUpdate(second_sat_id);

        first_module->disconnectDyn(DynamicGateType::ISL, second_sat_id);
        second_module->disconnectDyn(DynamicGateType::ISL, first_sat_id);
    }
}

void TopologyControlBase::connect_satellite_groundstation(RoutingTopology &topo, bool connect_modules, size_t sat_id, size_t gs_id) const {
    double distance = topo.calculateSatGsDistance(sat_id, gs_id);
    topo.connectGSL(gs_id, sat_id, distance);
    if (connect_modules) {
        auto sat_module = getSatelliteForUpdate(sat_id);
        auto gs_module = getGroundStationForUpdate(gs_id);
        double delay = distance / SPEED_OF_LIGHT;
        auto sat_inputGate = sat_module->getNextInputGate(DynamicGateType::GSL, gs_id);
        auto gs_inputGate = gs_module->getNextInputGate(DynamicGateType::GSL, sat_id);
        sat_module->connectToDyn(DynamicGateType::GSL, gs_id, gs_inputGate, delay, gsl_datarate);
        gs_module->connectToDyn(DynamicGateType::GSL, sat_id, sat_inputGate, delay, gsl_datarate);
    }
}

void TopologyControlBase::disconnect_satellite_groundstation(RoutingTopology &topo, bool connect_modules, size_t sat_id, size_t gs_id) const {
    topo.disconnectGSL(gs_id, sat_id);
    if (connect_modules) {
        auto sat_module = getSatelliteForUpdate(sat_id);
        auto gs_module = getGroundStationForUpdate(gs_id);
        sat_module->disconnectDyn(DynamicGateType::GSL, gs_id);
        gs_module->disconnectDyn(DynamicGateType::GSL, sat_id);
    }
}

}  // namespace florasat
