/*
 * TopologyControl.cc
 *
 * Created on: Dec 20, 2022
 *     Author: Robin Ohs
 */

#include "ContactPlanTopologyControl.h"
#include <cmath>

namespace florasat {
namespace topologycontrol {

Define_Module(ContactPlanTopologyControl);

void ContactPlanTopologyControl::initialize(int stage) {
    TopologyControlBase::initialize(stage);
    if (stage == inet::INITSTAGE_LOCAL) {
        maxRangeIsl = par("maxRangeIsl");
        maxRangeGs = par("maxRangeGs");
        contactPlan = check_and_cast<ContactPlan *>(getParentModule()->getSubmodule("contactPlan"));
        ASSERT(contactPlan != nullptr);
    }
}

void ContactPlanTopologyControl::finish(){
    endOfSim = true;
    if (!interPlaneIslDisabled)
        updateInterSatelliteLinks();
    contactPlan->writeContactPlan();
}

void ContactPlanTopologyControl::updateTopology() {
    core::Timer timer = core::Timer();
    // update ISL links and groundlinks
    topologyChanged = false;
    /*if (!interPlaneIslDisabled){
        updateInterSatelliteLinks();
    }*/
    updateInterSatelliteLinks();
    updateGroundstationLinks();

    EV << "TC: Calculation took " << timer.getTime() / 1000 / 1000 << "ms" << endl;
    // if there was any change to the topology, track current contacts
    if (topologyChanged)
        trackTopologyChange();
}


/**
 * Updates the links between each Groundstation and Satellites based on a ContactPlan instance.
 */
void ContactPlanTopologyControl::updateGroundstationLinks() {
    EV << "updateGroundstationLinks: starting" << endl;
    for (size_t gsId = 0; gsId < numGroundStations; gsId++) {
        GroundStationRoutingBase *gs = groundStations.at(gsId);
        for (size_t satId = 0; satId < numSatellites; satId++) {
            SatelliteRoutingBase *sat = satellites.at(satId);
            if (isGroundStationContactValid(gs, sat) && contactStarts.count(std::pair<int, int>(gsId, getShiftedSatelliteId(satId))) == 0){
                EV << "Starting Contact: " << endl;
                if (simTime().dbl() != 0.0) {
                    contactStarts.emplace(std::pair<int, int>(gsId, getShiftedSatelliteId(satId)), simTime().dbl());
                }
            } else if (!isGroundStationContactValid(gs, sat) && contactStarts.count(std::pair<int, int>(gsId, getShiftedSatelliteId(satId))) > 0) {
                EV << "Ending Contact: " << endl;
                double startTime = contactStarts.at(std::pair<int, int>(gsId, getShiftedSatelliteId(satId)));
                double endTime = simTime().dbl();
                contactPlan->addContact(startTime, endTime, gsId + 1, getShiftedSatelliteId(satId), 1000, 1);
                contactPlan->addContact(startTime, endTime, getShiftedSatelliteId(satId), gsId + 1, 1000, 1);
                contactPlan->addRange(startTime, endTime, gsId + 1, getShiftedSatelliteId(satId), 0, 1);
                contactStarts.erase(std::pair<int, int>(gsId, getShiftedSatelliteId(satId)));
            }
        }
    }
    contactPlan->updateContactRanges();
    contactPlan->sortContactIdsBySrcByStartTime();
    contactPlan->printContactPlan();
}


std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*> ContactPlanTopologyControl::getSamePlaneNeighbors(SatelliteRoutingBase *sat){

    SatelliteRoutingBase* satUp = nullptr;
    SatelliteRoutingBase* satDown = nullptr;
    if (satsPerPlane == 1) return std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*>(satUp, satDown);

    int satId = sat->getId();
    int satNumberinPlane = sat->getNumberInPlane();

    if (satNumberinPlane == 0){
        satUp = satellites.at(satId + 1);
        satDown = satellites.at(satId + satsPerPlane - 1);
    }

    else if (satNumberinPlane == satsPerPlane-1){
        satUp = satellites.at(satId - satsPerPlane + 1);
        satDown = satellites.at(satId - 1);
    }

    else{
        satUp = satellites.at(satId + 1);
        satDown = satellites.at(satId - 1);
    }

    return std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*>(satUp, satDown);
}

std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*> ContactPlanTopologyControl::getAdyadenctPlaneNeighbors(SatelliteRoutingBase *sat){
    SatelliteRoutingBase* satLeft = nullptr;
    SatelliteRoutingBase* satRight = nullptr;
    if (planeCount == 1) return std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*>(satLeft, satRight);

    int satId = sat->getId();
    int satPlane = sat->getPlane();

    if (satPlane == 0){
        satLeft = satellites.at(satId + satsPerPlane * (planeCount-1));
        satRight = satellites.at(satId + satsPerPlane);
    }

    else if (satPlane == planeCount-1){
        satLeft = satellites.at(satId - satsPerPlane);
        satRight = satellites.at(satId - satsPerPlane * (planeCount-1));
    }

    else{
        satLeft = satellites.at(satId - satsPerPlane);
        satRight = satellites.at(satId + satsPerPlane);
    }

    return std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*>(satLeft, satRight);
}

void ContactPlanTopologyControl::updateInterSatelliteLinks() {
    EV << "updateInterSatelliteLinks: starting" << endl;
    if (simTime().dbl() > 0) {
        for (size_t satId = 0; satId < numSatellites; satId++) {
            SatelliteRoutingBase *sat = satellites.at(satId);

            std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*> samePlaneNeighbors = getSamePlaneNeighbors(sat);
            SatelliteRoutingBase* satUp = samePlaneNeighbors.first;
            SatelliteRoutingBase* satDown = samePlaneNeighbors.second;
            std::pair<SatelliteRoutingBase*, SatelliteRoutingBase*> adyadenctPlaneNeighbors = getAdyadenctPlaneNeighbors(sat);
            SatelliteRoutingBase* satLeft = adyadenctPlaneNeighbors.first;
            SatelliteRoutingBase* satRight = adyadenctPlaneNeighbors.second;

            int shiftedSat = getShiftedSatelliteId(sat->getId());
            int shiftedSatDown = -1;
            int shiftedSatUp = -1;
            int shiftedSatRight = -1;
            int shiftedSatLeft = -1;

            if (satDown != nullptr){
               int shiftedSatDown = getShiftedSatelliteId(satDown->getId());
               // Sat Down Connection
               if (isIslContactValid(sat, satDown) && (contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatDown)) == 0 || contactStarts.count(std::pair<int, int>(shiftedSatDown, shiftedSat)) == 0)){
                   connectSatellites(sat, satDown, isldirection::Direction::ISL_DOWN);
                   contactStarts.emplace(std::pair<int, int>(shiftedSat, shiftedSatDown), simTime().dbl());
               } else if ((!isIslContactValid(sat, satDown) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatDown)) > 0) {
                   EV << "Ending Contact Sats: " << endl;
                   double startTime = contactStarts.at(std::pair<int, int>(shiftedSat, shiftedSatDown));
                   double endTime = simTime().dbl();
                   contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatDown, 1000, 1);
                   contactPlan->addContact(startTime, endTime, shiftedSatDown,shiftedSat, 1000, 1);
                   contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatDown, 0, 1);
                   contactStarts.erase(std::pair<int, int>(shiftedSat, shiftedSatDown));
                   disconnectSatellites(sat, satDown, isldirection::Direction::ISL_DOWN);
               } else if ((!isIslContactValid(sat, satDown) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSatDown, shiftedSat)) > 0) {
                   EV << "Ending Contact Sats: " << endl;
                   double startTime = contactStarts.at(std::pair<int, int>(shiftedSatDown, shiftedSat));
                   double endTime = simTime().dbl();
                   contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatDown, 1000, 1);
                   contactPlan->addContact(startTime, endTime, shiftedSatDown,shiftedSat, 1000, 1);
                   contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatDown, 0, 1);
                   contactStarts.erase(std::pair<int, int>(shiftedSatDown, shiftedSat));
                   disconnectSatellites(sat, satDown, isldirection::Direction::ISL_DOWN);
               }
            }
            if (satUp != nullptr){
                int shiftedSatUp = getShiftedSatelliteId(satUp->getId());
                // Sat Up Connection
                if (isIslContactValid(sat, satUp) && (contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatUp)) == 0 || contactStarts.count(std::pair<int, int>(shiftedSatUp, shiftedSat)) == 0)){
                    connectSatellites(sat, satUp, isldirection::Direction::ISL_UP);
                    contactStarts.emplace(std::pair<int, int>(shiftedSat, shiftedSatUp), simTime().dbl());
                } else if ((!isIslContactValid(sat, satUp) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatUp)) > 0) {
                    EV << "Ending Contact Sats: " << endl;
                    double startTime = contactStarts.at(std::pair<int, int>(shiftedSat, shiftedSatUp));
                    double endTime = simTime().dbl();
                    contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatUp, 1000, 1);
                    contactPlan->addContact(startTime, endTime, shiftedSatUp,shiftedSat, 1000, 1);
                    contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatUp, 0, 1);
                    contactStarts.erase(std::pair<int, int>(shiftedSat, shiftedSatUp));
                    disconnectSatellites(sat, satUp, isldirection::Direction::ISL_UP);
                } else if ((!isIslContactValid(sat, satUp) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSatUp, shiftedSat)) > 0) {
                    EV << "Ending Contact Sats: " << endl;
                    double startTime = contactStarts.at(std::pair<int, int>(shiftedSatUp, shiftedSat));
                    double endTime = simTime().dbl();
                    contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatUp, 1000, 1);
                    contactPlan->addContact(startTime, endTime, shiftedSatUp,shiftedSat, 1000, 1);
                    contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatUp, 0, 1);
                    contactStarts.erase(std::pair<int, int>(shiftedSatUp, shiftedSat));
                    disconnectSatellites(sat, satUp, isldirection::Direction::ISL_UP);
                }
            }
            if (satRight != nullptr){
                int shiftedSatRight = getShiftedSatelliteId(satRight->getId());
                // Sat Right Connection
                if (isIslContactValid(sat, satRight) && (contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatRight)) == 0 || contactStarts.count(std::pair<int, int>(shiftedSatRight, shiftedSat)) == 0)){
                    connectSatellites(sat, satRight, isldirection::Direction::ISL_RIGHT);
                    contactStarts.emplace(std::pair<int, int>(shiftedSat, shiftedSatRight), simTime().dbl());
                } else if ((!isIslContactValid(sat, satRight) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatRight)) > 0) {
                    EV << "Ending Contact Sats: " << endl;
                    double startTime = contactStarts.at(std::pair<int, int>(shiftedSat, shiftedSatRight));
                    double endTime = simTime().dbl();
                    contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatRight, 1000, 1);
                    contactPlan->addContact(startTime, endTime, shiftedSatRight,shiftedSat, 1000, 1);
                    contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatRight, 0, 1);
                    contactStarts.erase(std::pair<int, int>(shiftedSat, shiftedSatRight));
                    disconnectSatellites(sat, satRight, isldirection::Direction::ISL_RIGHT);
                } else if ((!isIslContactValid(sat, satRight) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSatRight, shiftedSat)) > 0) {
                    EV << "Ending Contact Sats: " << endl;
                    double startTime = contactStarts.at(std::pair<int, int>(shiftedSatRight, shiftedSat));
                    double endTime = simTime().dbl();
                    contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatRight, 1000, 1);
                    contactPlan->addContact(startTime, endTime, shiftedSatRight,shiftedSat, 1000, 1);
                    contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatRight, 0, 1);
                    contactStarts.erase(std::pair<int, int>(shiftedSatRight, shiftedSat));
                    disconnectSatellites(sat, satRight, isldirection::Direction::ISL_RIGHT);
                }
            }
            if (satLeft != nullptr){
                int shiftedSatLeft = getShiftedSatelliteId(satLeft->getId());
                // Sat Left Connection
                if (isIslContactValid(sat, satLeft) && (contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatLeft)) == 0 || contactStarts.count(std::pair<int, int>(shiftedSatLeft, shiftedSat)) == 0)){
                    connectSatellites(sat, satLeft, isldirection::Direction::ISL_LEFT);
                    contactStarts.emplace(std::pair<int, int>(shiftedSat, shiftedSatLeft), simTime().dbl());
                } else if ((!isIslContactValid(sat, satLeft) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSat, shiftedSatLeft)) > 0) {
                    EV << "Ending Contact Sats: " << endl;
                    double startTime = contactStarts.at(std::pair<int, int>(shiftedSat, shiftedSatLeft));
                    double endTime = simTime().dbl();
                    contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatLeft, 1000, 1);
                    contactPlan->addContact(startTime, endTime, shiftedSatLeft,shiftedSat, 1000, 1);
                    contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatLeft, 0, 1);
                    contactStarts.erase(std::pair<int, int>(shiftedSat, shiftedSatLeft));
                    disconnectSatellites(sat, satLeft, isldirection::Direction::ISL_LEFT);
                } else if ((!isIslContactValid(sat, satLeft) || endOfSim) && contactStarts.count(std::pair<int, int>(shiftedSatLeft, shiftedSat)) > 0) {
                    EV << "Ending Contact Sats: " << endl;
                    double startTime = contactStarts.at(std::pair<int, int>(shiftedSatLeft, shiftedSat));
                    double endTime = simTime().dbl();
                    contactPlan->addContact(startTime, endTime, shiftedSat, shiftedSatLeft, 1000, 1);
                    contactPlan->addContact(startTime, endTime, shiftedSatLeft,shiftedSat, 1000, 1);
                    contactPlan->addRange(startTime, endTime, shiftedSat, shiftedSatLeft, 0, 1);
                    contactStarts.erase(std::pair<int, int>(shiftedSatLeft, shiftedSat));
                    disconnectSatellites(sat, satLeft, isldirection::Direction::ISL_LEFT);
                }
            }
        }
        contactPlan->updateContactRanges();
        contactPlan->sortContactIdsBySrcByStartTime();
        contactPlan->printContactPlan();
    }
}

bool ContactPlanTopologyControl::isGroundStationContactValid(GroundStationRoutingBase *gs, SatelliteRoutingBase *sat){
    double distance = sat->getDistance(*gs);
    EV << "Distance: " << distance << " (KM)" << " GS: " << gs->getId() + 1 << " Sat: " << getShiftedSatelliteId(sat->getId()) + 1 << endl;
    return distance < maxRangeGs;
}

bool ContactPlanTopologyControl::isIslContactValid(SatelliteRoutingBase *sat1, SatelliteRoutingBase *sat2){
    double distance = sat1->getDistance(*sat2);
    EV << "Distance: " << distance << " (KM)" << " Sat 1: " << getShiftedSatelliteId(sat1->getId()) + 1 << " Sat 2: " << getShiftedSatelliteId(sat2->getId()) + 1 << endl;
    return distance < maxRangeIsl;
}


int ContactPlanTopologyControl::getShiftedSatelliteId(int satId) {
    return numGroundStations + satId + 1;
}

}  // namespace topologycontrol
}  // namespace flora
