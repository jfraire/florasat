/*
 * DtnTopologyControl.h
 *
 * Created on: Jun 16, 2023
 *     Author: Sebastian Montoya
 */

#pragma once

#include "orbit/constellation/topologycontrol/TopologyControlBase.h"
#include "dtn/contactplan/ContactPlan.h"

namespace florasat {

class ContactPlanTopologyControl : public TopologyControlBase {

    public:
     virtual void finish() override;
     bool endOfSim = false;

    protected:
     virtual void initialize(int stage) override;
     virtual void updateTopology() override;

     /** @brief Structs that represent connections between nodes, groundstations or satellites */
     std::map<std::pair<int, int>, double> contactStarts;

    private:
     void updateInterSatelliteLinks();
     void updateGroundstationLinks();
     int getShiftedSatelliteId(int satId);
     int maxRangeIsl;
     int maxRangeGs;
     ContactPlan *contactPlan;
     bool isGroundStationContactValid(GroundStationRoutingBase *gs, SatelliteRoutingBase *sat);
     bool isIslContactValid(SatelliteRoutingBase *sat1, SatelliteRoutingBase *sat2);

     std::pair<SatelliteRoutingBase *, SatelliteRoutingBase *> getSamePlaneNeighbors(SatelliteRoutingBase *sat);
     std::pair<SatelliteRoutingBase *, SatelliteRoutingBase *> getAdyadenctPlaneNeighbors(SatelliteRoutingBase *sat);
};

}  // namespace florasat
