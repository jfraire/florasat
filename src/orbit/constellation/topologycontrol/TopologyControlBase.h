/*
 * TopologyControlBase.h
 *
 * Created on: May 05, 2023
 * Updated on: Jan 01, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>
#include <optional>

#include "inet/common/INETDefs.h"
#include "inet/common/clock/ClockUserModuleMixin.h"

#include "core/common/Timer.h"
#include "core/macros/Validate.h"
#include "ground/station/GroundStation.h"
#include "orbit/constellation/ConstellationType.h"
#include "orbit/satellite/base/Satellite.h"
#include "networklayer/core/RoutingTopology.h"

namespace florasat {

using namespace omnetpp;
using namespace inet;

class TopologyControlBase : public ClockUserModuleMixin<cSimpleModule> {
   public:
    /**
     * @return The RoutingTopology valid at the current simulation time.
     */
    const RoutingTopology& get_topology() const;

    /**
     * @param time The simtime_t at which the RoutingTopology is desired.
     * @return The RoutingTopology for the given timestamp.
     */
    const RoutingTopology get_topology_at(simtime_t time) const;

    const GroundStation* getGroundStation(size_t gs_id) const {
        ASSERT(gs_id >= 0 && gs_id < m_groundstations.size());
        return m_groundstations[gs_id];
    }

    GroundStation* getGroundStationForUpdate(size_t gs_id) const {
        ASSERT(gs_id >= 0 && gs_id < m_groundstations.size());
        return m_groundstations[gs_id];
    }

    const Satellite* getSatellite(size_t sat_id) const {
        ASSERT(sat_id >= 0 && sat_id < m_satellites.size());
        return m_satellites[sat_id];
    }

    Satellite* getSatelliteForUpdate(size_t sat_id) const {
        ASSERT(sat_id >= 0 && sat_id < m_satellites.size());
        return m_satellites[sat_id];
    }

    ConstellationType getConstellationType() const {
        return constellation_type;
    }

    size_t calculate_satellite_id(size_t plane, size_t id_in_plane) const {
        ASSERT(plane < plane_count && plane >= 0);
        ASSERT(id_in_plane < sats_per_plane && id_in_plane >= 0);
        return sats_per_plane * plane + id_in_plane;
    }

    size_t get_number_of_groundstations() const {
        return m_groundstations.size();
    }

    size_t get_number_of_satellites() const {
        return m_satellites.size();
    }

    /**
     * Calculates the remaining visibility between a ground station and satellite.
     *
     * @param gs_id the id of the ground station.
     * @param sat_id the id of the satellite.
     * @return A double with the length of the remaining visibility in seconds or std::nullopt if not connected.
     */
    std::optional<double> calculate_remaining_visibility(size_t gs_id, size_t sat_id) const;

   protected:
    ~TopologyControlBase();
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    virtual void handleMessage(cMessage* msg) override;
    virtual void initialize_topology();
    virtual void update_topology();
    virtual void validate_continuous_connectivity();

    virtual void load_satellites();
    virtual void load_groundstations();
    virtual void load_satellite_positions();

    virtual RoutingTopology create_new_unconnected_topo(simtime_t time) const;

    virtual void connect_satellites(RoutingTopology& topo, bool connect_modules, size_t first_sat, size_t second_sat) const;
    virtual void disconnect_satellites(RoutingTopology& topo, bool connect_modules, size_t first_sat, size_t second_sat) const;

    virtual void connect_satellite_groundstation(RoutingTopology& topo, bool connect_modules, size_t sat_id, size_t gs_id) const;
    virtual void disconnect_satellite_groundstation(RoutingTopology& topo, bool connect_modules, size_t sat_id, size_t gs_id) const;

    // unimplemented methods
    virtual void update_intra_plane_isls(RoutingTopology& topo, bool connect_modules) const = 0;
    virtual void update_inter_plane_isls(RoutingTopology& topo, bool connect_modules) const = 0;
    virtual void update_groundstation_links(RoutingTopology& topo, bool connect_modules) const = 0;

   protected:
    // cached ptrs

    // parameters
    clocktime_t update_interval;
    int sat_count;
    int gs_count;
    int topology_size;
    int plane_count;
    int sats_per_plane;
    int inter_plane_spacing;

    inet::units::values::bps isl_datarate;
    inet::units::values::bps gsl_datarate;
    double min_elevation_rad;

    // state
    ConstellationType constellation_type;
    ClockEvent* selfMsg = nullptr;
    std::vector<Satellite*> m_satellites;
    std::vector<GroundStation*> m_groundstations;
    RoutingTopology m_topology = RoutingTopology({}, {});
    cJulian* m_time = nullptr;
};

}  // namespace florasat
