/*
 * DynamicTopologyControl.h
 *
 * Created on: Jan 08, 2023
 *     Author: Robin Ohs
 */

#pragma once

#include "orbit/constellation/topologycontrol/TopologyControlBase.h"

namespace florasat {

using namespace omnetpp;
using namespace inet;

class DynamicTopologyControl : public TopologyControlBase {
   protected:
    virtual void update_intra_plane_isls(RoutingTopology& topo, bool connect_modules) const override;
    virtual void update_inter_plane_isls(RoutingTopology& topo, bool connect_modules) const override;
    virtual void update_groundstation_links(RoutingTopology& topo, bool connect_modules) const override;
};

}  // namespace florasat
