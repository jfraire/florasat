/*
 * DynamicTopologyControl.cc
 *
 * Created on: May 05, 2023
 *     Author: Robin Ohs
 */

#include "DynamicTopologyControl.h"

namespace florasat {

Register_Class(DynamicTopologyControl);

void DynamicTopologyControl::update_intra_plane_isls(RoutingTopology &topo, bool connect_modules) const {
    for (size_t p = 0; p < plane_count; p++) {
        for (size_t q = 0; q < sats_per_plane; q++) {
            size_t first_sat_id = calculate_satellite_id(p, q);
            size_t second_sat_id;
            if (q == sats_per_plane - 1) {
                second_sat_id = calculate_satellite_id(p, 0);
            } else {
                second_sat_id = calculate_satellite_id(p, q + 1);
            }
            connect_satellites(topo, connect_modules, first_sat_id, second_sat_id);
        }
    }
}

void DynamicTopologyControl::update_inter_plane_isls(RoutingTopology &topo, bool connect_modules) const {
    for (size_t p = 0; p < plane_count; p++) {
        for (size_t q = 0; q < sats_per_plane; q++) {
            size_t first_sat_id = calculate_satellite_id(p, q);
            bool is_last_plane = p == plane_count - 1;
            if (constellation_type == ConstellationType::WALKER_DELTA) {
                size_t second_sat_id;
                if (is_last_plane) {
                    second_sat_id = calculate_satellite_id(0, (q + inter_plane_spacing) % sats_per_plane);
                } else {
                    second_sat_id = calculate_satellite_id(p + 1, q);
                }
                connect_satellites(topo, connect_modules, first_sat_id, second_sat_id);
            } else if (constellation_type == ConstellationType::WALKER_STAR) {
                if (is_last_plane)
                    break;
                size_t second_sat_id = calculate_satellite_id(p + 1, q);

                auto first_module = getSatelliteForUpdate(first_sat_id);
                auto second_module = getSatelliteForUpdate(second_sat_id);

                if (abs(first_module->getLatitude()) > 70.0 || abs(second_module->getLatitude()) > 70.0) {
                    // EV << "Disconnect " << first_sat_id << " and " << second_sat_id << " due to lat > 70°" << endl;
                    disconnect_satellites(topo, connect_modules, first_sat_id, second_sat_id);
                } else if (first_module->isAscending() != second_module->isAscending()) {
                    // EV << "Disconnect " << first_sat_id << " and " << second_sat_id << " due to not both asc or desc" << endl;
                    disconnect_satellites(topo, connect_modules, first_sat_id, second_sat_id);
                } else {
                    connect_satellites(topo, connect_modules, first_sat_id, second_sat_id);
                }
            } else {
                error("Unhandled/Unsupported constellation type %u", static_cast<int>(constellation_type));
            }
        }
    }
}

void DynamicTopologyControl::update_groundstation_links(RoutingTopology &topo, bool connect_modules) const {
    for (auto sat : m_satellites) {
        for (auto gs : m_groundstations) {
            size_t sat_id = sat->getIndex();
            size_t gs_id = gs->getIndex();
            double elevation = topo.calculateElevation(sat_id, gs_id);
            if (elevation >= min_elevation_rad) {
                connect_satellite_groundstation(topo, connect_modules, sat_id, gs_id);
            } else {
                disconnect_satellite_groundstation(topo, connect_modules, sat_id, gs_id);
            }
        }
    }
}

}  // namespace florasat
