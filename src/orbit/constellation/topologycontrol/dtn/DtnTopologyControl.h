/*
 * DtnTopologyControl.h
 *
 * Created on: May 17, 2023
 *     Author: Sebastian Montoya
 * Updated on: Jan 08, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <iostream>
#include <map>
#include <utility>

#include "orbit/constellation/topologycontrol/TopologyControlBase.h"
#include "dtn/contactplan/ContactPlan.h"

using namespace omnetpp;

namespace florasat {

class DtnTopologyControl : public TopologyControlBase {
   public:
   protected:
    virtual void update_intra_plane_isls(RoutingTopology& topo, bool connect_modules) const override;
    virtual void update_inter_plane_isls(RoutingTopology& topo, bool connect_modules) const override;
    virtual void update_groundstation_links(RoutingTopology& topo, bool connect_modules) const override;

    bool isDtnContactStarting(Contact contact) const;
    bool isDtnContactTakingPlace(int gsId, int satId, Contact contact) const;
    bool isDtnContactEnding(int gsId, int satId, Contact contact) const;
    int getShiftedSatelliteId(int satId) const;
};

}  // namespace florasat
