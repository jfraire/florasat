/*
 * DtnTopologyControl.cc
 *
 * Created on: May 17, 2023
 *     Author: Sebastian Montoya
 * Updated on: Jan 08, 2024
 *     Author: Robin Ohs
 */

#include "DtnTopologyControl.h"

namespace florasat {

Define_Module(DtnTopologyControl);

void DtnTopologyControl::update_intra_plane_isls(RoutingTopology &topo, bool connect_modules) const {
    ContactPlan *contactPlan = check_and_cast<ContactPlan *>(getParentModule()->getSubmodule("contactPlan"));
    ASSERT(contactPlan != nullptr);
    // iterate over planes
    for (size_t plane = 0; plane < plane_count; plane++) {
        // iterate over sats in plane
        for (size_t planeSat = 0; planeSat < sats_per_plane; planeSat++) {
            int index = planeSat + plane * sats_per_plane;
            int isLastSatInPlane = index % sats_per_plane == sats_per_plane - 1;

            // get the two satellites we want to connect.
            // If we have the last in plane, we connect it to the first of the plane
            Satellite *curSat = m_satellites.at(index);
            ASSERT(curSat != nullptr);
            int nextId = isLastSatInPlane ? plane * sats_per_plane : index + 1;
            Satellite *otherSat = m_satellites.at(nextId);
            ASSERT(otherSat != nullptr);

            int numGroundStations = m_groundstations.size();

            vector<Contact> contactsBetweenSats = contactPlan->getContactsBySrcDst(numGroundStations + curSat->getId(), numGroundStations + otherSat->getId());
            for (size_t satContactIndex = 0; satContactIndex < contactsBetweenSats.size(); satContactIndex++) {
                int shiftedCurSatId = getShiftedSatelliteId(curSat->getId());
                int shiftedOtherSatId = getShiftedSatelliteId(otherSat->getId());
                if (isDtnContactTakingPlace(shiftedCurSatId, shiftedOtherSatId, contactsBetweenSats.at(satContactIndex))) {
                    connect_satellites(topo, connect_modules, curSat->getId(), otherSat->getId());
                }
            }
        }
    }
}

void DtnTopologyControl::update_inter_plane_isls(RoutingTopology &topo, bool connect_modules) const {
    ContactPlan *contactPlan = check_and_cast<ContactPlan *>(getParentModule()->getSubmodule("contactPlan"));
    ASSERT(contactPlan != nullptr);

    int numSatellites = m_satellites.size();
    int numGroundStations = m_groundstations.size();

    for (size_t satId = 0; satId < numSatellites; satId++) {
        vector<Contact> satContacts = contactPlan->getContactsBySrc(getShiftedSatelliteId(satId));
        for (size_t contactIndex = 0; contactIndex < satContacts.size(); contactIndex++) {
            Contact contact = satContacts.at(contactIndex);
            if (contact.getSourceEid() >= numGroundStations + 1 && contact.getDestinationEid() >= numGroundStations + 1) {
                int unshiftedCurSatId = contact.getSourceEid() - numGroundStations - 1;
                int unshiftedOtherSatId = contact.getDestinationEid() - numGroundStations - 1;
                Satellite *curSat = m_satellites.at(unshiftedCurSatId);
                Satellite *otherSat = m_satellites.at(unshiftedOtherSatId);
                if (isDtnContactStarting(contact)) {
                    connect_satellites(topo, connect_modules, curSat->getId(), otherSat->getId());
                } else if (isDtnContactEnding(contact.getSourceEid(), contact.getDestinationEid(), contact)) {
                    EV << "Disconnecting : " << contact.getSourceEid() << " " << contact.getDestinationEid() << endl;
                    disconnect_satellites(topo, connect_modules, curSat->getId(), otherSat->getId());
                }
            }
        }
    }
}

/**
 * Updates the links between each Groundstation and Satellites based on a ContactPlan instance.
 */
void DtnTopologyControl::update_groundstation_links(RoutingTopology &topo, bool connect_modules) const {
    ContactPlan *contactPlan = check_and_cast<ContactPlan *>(getParentModule()->getSubmodule("contactPlan"));
    if (contactPlan == nullptr) {
        error("Error in DtnTopologyControl::updateGroundstationLinksDtn(): contactPlan is nullptr. Make sure the module exists.");
    }

    int numSatellites = m_satellites.size();
    int numGroundStations = m_groundstations.size();

    for (size_t gsId = 1; gsId < numGroundStations + 1; gsId++) {
        for (size_t satId = 0; satId < numSatellites; satId++) {
            vector<Contact> satContacts = contactPlan->getContactsBySrcDst(gsId, getShiftedSatelliteId(satId));
            for (size_t i = 0; i < satContacts.size(); i++) {
                Contact contact = satContacts.at(i);
                int shiftedSatId = getShiftedSatelliteId(satId);
                // EV << "Shifted Sat ID: " << shiftedSatId << " Ground Station ID: " << gsId << endl;
                if (isDtnContactStarting(contact)) {
                    connect_satellite_groundstation(topo, connect_modules, satId, gsId - 1);
                } else if (isDtnContactTakingPlace(gsId, shiftedSatId, contact)) {
                    connect_satellite_groundstation(topo, connect_modules, satId, gsId - 1);
                } else if (isDtnContactEnding(gsId, shiftedSatId, contact)) {
                    disconnect_satellite_groundstation(topo, connect_modules, satId, gsId - 1);
                }
            }
        }
    }
}

/**
 * Checks if a contact between a GroundStation and Satellite is starting based on a Contact instance
 *
 * @param gsInfo contains data related to a GroundStation
 * @param satInfo contains data related to a Satellite
 * @param contact represent a Contact between two nodes in a Contact Plan
 * @return whether a contact between a GroundStation and a Satellite is starting
 */
bool DtnTopologyControl::isDtnContactStarting(Contact contact) const {
    return contact.getStart() == simTime().dbl();
}

/**
 * Checks if a contact between a GroundStation and Satellite is taking place based on a Contact instance
 *
 * @param gsInfo contains data related to a GroundStation
 * @param satInfo contains data related to a Satellite
 * @param contact represent a Contact between two nodes in a Contact Plan
 * @return whether a contact between a GroundStation and a Satellite is taking place
 */
bool DtnTopologyControl::isDtnContactTakingPlace(int gsId, int satId, Contact contact) const {
    return contact.getSourceEid() == gsId && contact.getDestinationEid() == satId && contact.getEnd() > simTime().dbl() && contact.getStart() < simTime().dbl();
}

/**
 * Checks if a contact between a GroundStation and Satellite is ending based on a Contact instance
 *
 * @param gsInfo contains data related to a GroundStation
 * @param satInfo contains data related to a Satellite
 * @param contact represent a Contact between two nodes in a Contact Plan
 * @return whether a contact between a GroundStation and a Satellite is ending
 */
bool DtnTopologyControl::isDtnContactEnding(int gsId, int satId, Contact contact) const {
    return contact.getSourceEid() == gsId && contact.getDestinationEid() == satId && contact.getEnd() == simTime().dbl();
}

int DtnTopologyControl::getShiftedSatelliteId(int satId) const {
    int numGroundStations = m_groundstations.size();
    return numGroundStations + satId + 1;
}

}  // namespace florasat
