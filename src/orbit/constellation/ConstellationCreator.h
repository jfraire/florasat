/*
 * ConstellationCreator.h
 *
 * Created on: Jan 21, 2023
 * Updated on: Jan 05, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include <string>

#include "core/macros/Validate.h"
#include "inet/common/INETDefs.h"
#include "core/mobility/libnorad/cOrbitA.h"
#include "core/mobility/satellite/SatelliteMobility.h"

using namespace omnetpp;

namespace florasat {

class ConstellationCreator : public cSimpleModule {
   protected:
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

   private:
    void create_constellation();
    void configure_satellite(int index, double raan, double mean_anomaly,
                             int plane_index);

   private:
    // cached ptrs
    cModule *simulation_module = nullptr;

    // fields
    double inclination;
    int sat_count;
    int plane_count;
    int sats_per_plane;
    int inter_plane_spacing;
    double altitude_meter;
    double raan_spread;

    double bstar;
    double drag;

    int base_year;
    double base_day;

    double eccentricity;

    const char* satelliteType;
};

}  // namespace florasat
