/*
 * MobilityOrchestrator.cc
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#include "MobilityOrchestrator.h"

namespace florasat {

Define_Module(MobilityOrchestrator);

MobilityOrchestrator::~MobilityOrchestrator() {
    cancelAndDelete(selfMsg);
}

void MobilityOrchestrator::initialize(int stage) {
    ClockUserModuleMixin::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        update_interval = par("update_interval");
        selfMsg = new ClockEvent("sendTimer");
    } else if (stage == inet::INITSTAGE_CLOCK) {
        scheduleClockEventAfter(update_interval, selfMsg);
    } else if (stage == inet::INITSTAGE_GROUP_MOBILITY) {
        initialize_positions();
    }
}

void MobilityOrchestrator::handleMessage(cMessage* msg) {
    if (!msg->isSelfMessage()) {
        error("This module can only handle self-messages.");
    }
    rescheduleClockEventAfter(update_interval, selfMsg);
    update_positions();
}

void MobilityOrchestrator::initialize_positions() {
    int number_of_satellites = getSystemModule()->getSubmoduleVectorSize("satellites");

    satellite_mobilities.reserve(number_of_satellites);
    for (size_t i = 0; i < number_of_satellites; i++) {
        auto sat_mobility = check_and_cast<SatelliteMobility*>(getSystemModule()->getSubmodule("satellites", i)->getSubmodule("satellite_mobility"));
        satellite_mobilities.push_back(sat_mobility);
        // init position
        auto [latRad, lonRad, altMeters, is_ascending] = calculate_sat_position(0.0, sat_mobility);
        sat_mobility->updatePosition(latRad, lonRad, altMeters, is_ascending);
    }
}

void MobilityOrchestrator::update_positions() {
    // EV << "Update positions at " << simtime << endl;
    const double min_since_epoch = simTime().dbl() / 60;

    for (const auto sat_mobility : satellite_mobilities) {
        auto [latRad, lonRad, altMeters, is_ascending] = calculate_sat_position(min_since_epoch, sat_mobility);
        sat_mobility->updatePosition(latRad, lonRad, altMeters, is_ascending);
    }
}

std::tuple<double, double, double, bool> MobilityOrchestrator::calculate_sat_position(double min_since_epoch, SatelliteMobility* const sat) {
    auto orbit = sat->getOrbit();
    cEci eci = cEci();
    orbit->getPosition(min_since_epoch, eci);
    auto coord = eci.toGeo();
    // EV << "lla=" << coord.m_LatRad << "," << coord.m_LonRad << "," << coord.m_AltMeters << endl;

    // longitude
    double lonRad = coord.m_LonRad;
    // fix west not negative
    if (lonRad > PI) {
        lonRad -= TWOPI;
    }
    ASSERT(lonRad >= -TWOPI || lonRad <= TWOPI);
    // double lon = rad2deg(lonRad);

    // latitude
    double latRad = coord.m_LatRad;
    ASSERT(latRad >= -PI || latRad <= PI);
    // double lat = rad2deg(latRad);

    // altitude in meters
    double altMeters = coord.m_AltMeters;

    // is ascending (only works for almost circular orbits)
    double mn_anomaly = orbit->mnAnomaly(min_since_epoch * 60);
    bool is_ascending = mn_anomaly > ONEHALFPI || (mn_anomaly < HALFPI);

    // std::cout << "Lat" << lat << ";"
    //           << "Lon" << lon << ";"
    //           << "Alt" << alt << ";" << std::endl;

    return {latRad, lonRad, altMeters, is_ascending};
}

}  // namespace florasat