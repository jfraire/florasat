/*
 * MobilityOrchestrator.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include <omnetpp.h>

#include <string>

#include "core/common/Timer.h"
#include "core/macros/Validate.h"
#include "inet/common/INETDefs.h"
#include "inet/common/clock/ClockUserModuleMixin.h"
#include "core/mobility/libnorad/cEcef.h"
#include "core/mobility/libnorad/cEci.h"
#include "core/mobility/libnorad/cOrbitA.h"
#include "core/mobility/satellite/SatelliteMobility.h"

namespace florasat {

using namespace omnetpp;
using namespace inet;

class MobilityOrchestrator : public ClockUserModuleMixin<cSimpleModule> {
   public:
    ~MobilityOrchestrator();

   protected:
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    virtual void handleMessage(cMessage* msg) override;

    void initialize_positions();
    void update_positions();

    std::tuple<double, double, double, bool> calculate_sat_position(double time, SatelliteMobility* const sat);

   protected:
    // cached ptrs

    // parameters
    clocktime_t update_interval;

    // state
    std::vector<SatelliteMobility*> satellite_mobilities;
    ClockEvent* selfMsg = nullptr;
};

}  // namespace florasat
