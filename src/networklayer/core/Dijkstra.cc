/*
 * Dijkstra.cc
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#include "Dijkstra.h"

using namespace omnetpp;

namespace florasat {
namespace dijkstra {

namespace {

std::tuple<std::vector<size_t>, double> generatePath(RoutingTopology const& topology, size_t internal_id_start, size_t internal_id_end) {
    updatable_priority_queue<unsigned long, double> pQ;
    std::vector<std::optional<size_t>> prev(topology.size(), std::nullopt);
    pQ.set(internal_id_start, 0);
    std::optional<double> distance = std::nullopt;

    while (!pQ.empty()) {
        auto pq_node = pQ.pop_value(true);
        if (pq_node.key == internal_id_end) {
            // Path found
            distance = {pq_node.priority * -1};
            break;
        }
        for (auto const& edge : topology.getEdges(pq_node.key)) {
            size_t target_id = edge.getTo();
            // A satellite-groundstation-satellite hop is not allowed
            if (topology.isGroundstation(target_id) && target_id != internal_id_end) {
                continue;
            }
            bool updated = pQ.set(target_id, pq_node.priority - edge.getDistance(), true);
            if (updated) {
                prev[target_id] = {pq_node.key};
            }
        }
    }

    // path reconstruction
    std::deque<size_t> path;
    if (topology.isSatellite(internal_id_end)) {
        path.push_back(topology.calculateExternalSatId(internal_id_end));

    } else {
        path.push_back(topology.calculateExternalGsId(internal_id_end));
    }
    std::optional<size_t> node = {internal_id_end};
    while (node.has_value() && node.value() != internal_id_start) {
        node = prev[node.value()];
        if (topology.isSatellite(node.value())) {
            path.push_front(topology.calculateExternalSatId(node.value()));
        } else {
            path.push_front(topology.calculateExternalGsId(node.value()));
        }
    }

    std::vector<size_t> v(std::make_move_iterator(path.begin()),
                          std::make_move_iterator(path.end()));

    ASSERT(distance.has_value() && distance.value() > 0);

    return {v, distance.value()};
}

}  // namespace

std::tuple<G2GPath, double> generateG2GPath(RoutingTopology const& topology, size_t gs_id_start, size_t gs_id_end) {
    size_t from = topology.calculateInternalGsId(gs_id_start);
    size_t to = topology.calculateInternalGsId(gs_id_end);
    auto [path, distance] = generatePath(topology, from, to);
    return {G2GPath(path), distance};
}

// std::vector<size_t> generateSatGsPath(RoutingTopology const& topology, size_t sat_id_start, size_t gs_id_end) {
//     size_t from = topology.calculateInternalSatId(sat_id_start);
//     size_t to = topology.calculateInternalGsId(gs_id_end);
//     return generatePath(topology, from, to);
// }

// std::vector<size_t> generateGsSatPath(RoutingTopology const& topology, size_t gs_id_start, size_t sat_id_end) {
//     size_t from = topology.calculateInternalGsId(gs_id_start);
//     size_t to = topology.calculateInternalSatId(sat_id_end);
//     return generatePath(topology, from, to);
// }

// std::vector<size_t> generateSatSatPath(RoutingTopology const& topology, size_t sat_id_start, size_t sat_id_end) {
//     size_t from = topology.calculateInternalSatId(sat_id_start);
//     size_t to = topology.calculateInternalSatId(sat_id_end);
//     return generatePath(topology, from, to);
// }

std::tuple<S2SPath, double> generateS2SPath(RoutingTopology const& topology, size_t sat_id_start, size_t sat_id_end) {
    size_t from = topology.calculateInternalSatId(sat_id_start);
    size_t to = topology.calculateInternalSatId(sat_id_end);
    auto [path, distance] = generatePath(topology, from, to);
    return {S2SPath(path), distance};
}

}  // namespace dijkstra
}  // namespace florasat
