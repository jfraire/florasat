/*
 * SupportedConstellationType.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

namespace florasat {

enum class SupportedConstellationType {
    DELTA,
    STAR,
    BOTH
};

}  // namespace florasat
