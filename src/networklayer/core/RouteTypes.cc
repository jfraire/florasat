/*
 * RouteTypes.cc
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#include "RouteTypes.h"

namespace florasat {

/////////////

/* BasePath */

size_t BasePath::getNode(size_t index) const {
    ASSERT(index < m_path.size() && index >= 0);
    return m_path[index];
}

std::vector<size_t> const& BasePath::getFullPath() const {
    return m_path;
}

size_t BasePath::getNumberOfNodes() const {
    return m_path.size();
}

size_t BasePath::getNumberOfHops() const {
    return getNumberOfNodes() - 1;
}

/////////////

/* G2G PATH */

size_t G2GPath::getSrcGsId() const {
    return getNode(0);
}

size_t G2GPath::getDstGsId() const {
    return getNode(getNumberOfNodes() - 1);
}

std::span<size_t> const G2GPath::getSatellitePath() {
    return slice<1, -1>(m_path);
}

size_t G2GPath::getNumberOfSatelliteHops() const {
    return getNumberOfHops() - 2;
}

size_t G2GPath::getAccessSatellite() const {
    return getNode(1);
}

size_t G2GPath::getLastSatellite() const {
    return getNode(getNumberOfNodes() - 2);
}

/////////////

/* S2S PATH */

std::span<size_t> const S2SPath::getSatellitePath() {
    return slice<0, 0>(m_path);
}

size_t S2SPath::getNumberOfSatelliteHops() const {
    return getNumberOfHops();
}

size_t S2SPath::getSrcSatId() const {
    return getNode(0);
}

size_t S2SPath::getDstSatId() const {
    return getNode(getNumberOfNodes() - 1);
}

/////////////

/* S2G PATH */

std::span<size_t> const S2GPath::getSatellitePath() {
    return slice<0, -1>(m_path);
}

size_t S2GPath::getNumberOfSatelliteHops() const {
    return getNumberOfHops() - 1;
}

size_t S2GPath::getSrcSatId() const {
    return getNode(0);
}

size_t S2GPath::getLastSatellite() const {
    return getNode(getNumberOfNodes() - 2);
}

size_t S2GPath::getDstGsId() const {
    return getNode(getNumberOfNodes() - 1);
}

/////////////

}  // namespace florasat