/*
 * RoutingTopology.h
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#include "RoutingTopology.h"

namespace florasat {

//////////////////////////////////////////////////////////////////////
/** Edge */

Edge::Edge(size_t to, double distance) {
    m_to = to;
    m_distance = distance;
};

size_t Edge::getTo() const {
    return m_to;
}

double Edge::getDistance() const {
    return m_distance;
}

void Edge::updateDistance(double newDistance) {
    m_distance = newDistance;
}

//////////////////////////////////////////////////////////////////////
/** Node */

Node::Node(NodeType type, cVector position) {
    m_type = type;
    m_position = position;
};

Node::Node(NodeType type, double x, double y, double z) {
    m_type = type;
    m_position = cVector(x, y, z);
};

const cVector& Node::getPosition() const {
    return m_position;
}

void Node::updatePosition(double x, double y, double z) {
    m_position.m_x = x;
    m_position.m_y = y;
    m_position.m_z = z;
}

const std::vector<Edge>& Node::getEdges() const {
    return m_edges;
}

std::vector<Edge>& Node::getEdgesForUpdate() {
    return m_edges;
}

void Node::addEdge(size_t to, double distance) {
    m_edges.push_back(Edge(to, distance));
}

void Node::deleteEdge(size_t to) {
    std::erase_if(m_edges, [&to](const Edge& ele) {
        return ele.getTo() == to;
    });
}

bool Node::isGS() const {
    return m_type == NodeType::GS;
}

bool Node::isSat() const {
    return m_type == NodeType::SAT;
}

//////////////////////////////////////////////////////////////////////
/** Routing topology */

RoutingTopology::RoutingTopology(std::vector<Node> satellites, std::vector<Node> groundstations) {
    ASSERT(satellites.size() >= 0);
    ASSERT(groundstations.size() >= 0);
    m_sat_count = satellites.size();
    m_gs_count = groundstations.size();
    size_t m_topo_size = m_sat_count + m_gs_count;
    m_topo.reserve(m_topo_size);
    m_topo.insert(m_topo.end(), std::make_move_iterator(satellites.begin()), std::make_move_iterator(satellites.end()));
    m_topo.insert(m_topo.end(), std::make_move_iterator(groundstations.begin()), std::make_move_iterator(groundstations.end()));
}

void RoutingTopology::connectISL(size_t first_sat_id, size_t second_sat_id, double distance) {
    ASSERT(first_sat_id != second_sat_id);
    ASSERT(distance > 0);
    size_t first_id = calculateInternalSatId(first_sat_id);
    size_t second_id = calculateInternalSatId(second_sat_id);

    addOrUpdateEdge(first_id, second_id, distance);
    addOrUpdateEdge(second_id, first_id, distance);
}

void RoutingTopology::disconnectISL(size_t first_sat_id, size_t second_sat_id) {
    ASSERT(first_sat_id != second_sat_id);
    auto first_id = calculateInternalSatId(first_sat_id);
    auto second_id = calculateInternalSatId(second_sat_id);

    deleteEdge(first_id, second_id);
    deleteEdge(second_id, first_id);
}

void RoutingTopology::connectGSL(size_t gs_id, size_t sat_id, double distance) {
    auto first_id = calculateInternalGsId(gs_id);
    auto second_id = calculateInternalSatId(sat_id);
    ASSERT(first_id != second_id);

    addOrUpdateEdge(first_id, second_id, distance);
    addOrUpdateEdge(second_id, first_id, distance);
}

void RoutingTopology::disconnectGSL(size_t gs_id, size_t sat_id) {
    auto first_id = calculateInternalGsId(gs_id);
    auto second_id = calculateInternalSatId(sat_id);
    ASSERT(first_id != second_id);

    deleteEdge(first_id, second_id);
    deleteEdge(second_id, first_id);
}

void RoutingTopology::updatePosition(size_t sat_id, double x, double y, double z) {
    auto internal_sat_id = calculateInternalSatId(sat_id);
    m_topo[internal_sat_id].updatePosition(x, y, z);
}

const std::vector<Edge>& RoutingTopology::getEdges(size_t internal_id) const {
    return m_topo[internal_id].getEdges();
};

size_t RoutingTopology::calculateInternalSatId(size_t sat_id) const {
    ASSERT(sat_id >= 0 && sat_id < m_sat_count);
    return sat_id;
}

size_t RoutingTopology::calculateInternalGsId(size_t gs_id) const {
    ASSERT(gs_id >= 0 && gs_id < m_gs_count);
    size_t tmp = m_sat_count + gs_id;
    ASSERT(tmp >= m_sat_count && tmp < size());
    return tmp;
}

size_t RoutingTopology::calculateExternalSatId(size_t internal_sat_id) const {
    ASSERT(internal_sat_id >= 0 && internal_sat_id < m_sat_count);
    return internal_sat_id;
}

size_t RoutingTopology::calculateExternalGsId(size_t internal_gs_id) const {
    ASSERT(internal_gs_id >= m_sat_count && internal_gs_id < size());
    size_t gs_id = internal_gs_id - m_sat_count;
    ASSERT(gs_id >= 0 && gs_id < m_gs_count);
    return gs_id;
}

bool RoutingTopology::isSatellite(size_t internal_id) const {
    ASSERT(internal_id >= 0 && internal_id < size());
    return internal_id < m_sat_count;
}

bool RoutingTopology::isGroundstation(size_t internal_id) const {
    ASSERT(internal_id >= 0 && internal_id < size());
    return internal_id >= m_sat_count;
}

size_t RoutingTopology::size() const {
    return m_topo.size();
}

void RoutingTopology::print() const {
    for (size_t i = 0; i < size(); i++) {
        std::string prefix;
        size_t original_id;
        if (isSatellite(i)) {
            prefix = "Sat";
            original_id = calculateExternalSatId(i);
        } else {
            prefix = "Gs";
            original_id = calculateExternalGsId(i);
        }
        EV << prefix << " " << original_id << ":" << endl;
        for (auto const& edge : m_topo[i].getEdges()) {
            if (isSatellite(edge.getTo())) {
                prefix = "Sat";
                original_id = calculateExternalSatId(edge.getTo());
            } else {
                prefix = "GS";
                original_id = calculateExternalGsId(edge.getTo());
            }
            EV << "\t-> " << prefix << " " << original_id << "(" << edge.getDistance() << ")" << endl;
        }
    }
}

double RoutingTopology::calculateSatsDistance(size_t first_sat_id, size_t second_sat_id) const {
    size_t internal_first_sat_id = calculateInternalSatId(first_sat_id);
    size_t internal_second_sat_id = calculateInternalSatId(second_sat_id);
    const cVector& pos1 = m_topo[internal_first_sat_id].getPosition();
    const cVector& pos2 = m_topo[internal_second_sat_id].getPosition();
    return pos1.Distance(pos2);
};

double RoutingTopology::calculateSatGsDistance(size_t sat_id, size_t gs_id) const {
    size_t internal_sat_id = calculateInternalSatId(sat_id);
    size_t internal_gs_id = calculateInternalGsId(gs_id);
    const cVector& pos_sat = m_topo[internal_sat_id].getPosition();
    const cVector& pos_gs = m_topo[internal_gs_id].getPosition();
    return pos_sat.Distance(pos_gs);
};

double RoutingTopology::calculateElevation(size_t sat_id, size_t gs_id) const {
    size_t internal_sat_id = calculateInternalSatId(sat_id);
    size_t internal_gs_id = calculateInternalGsId(gs_id);
    const cVector& pos_sat = m_topo[internal_sat_id].getPosition();
    const cVector& pos_gs = m_topo[internal_gs_id].getPosition();

    cVector connectingVector(pos_gs.m_x - pos_sat.m_x, pos_gs.m_y - pos_sat.m_y, pos_gs.m_z - pos_sat.m_z);
    return connectingVector.Angle(pos_gs) - HALFPI;
};

std::optional<double> RoutingTopology::distanceInternal(size_t internal_first_id, size_t internal_second_id) {
    for (auto const& edge : m_topo[internal_first_id].getEdges()) {
        if (edge.getTo() == internal_second_id) {
            return edge.getDistance();
        }
    }
    return std::nullopt;
}

void RoutingTopology::addOrUpdateEdge(size_t internal_src_id, size_t internal_dst_id, double distance) {
    // iterate over edges and update if necessary
    for (auto& edge : m_topo[internal_src_id].getEdgesForUpdate()) {
        if (edge.getTo() == internal_dst_id) {
            edge.updateDistance(distance);
            return;
        }
    }

    // if this point is reached, no edge was found to destination -> Add new edge
    m_topo[internal_src_id].addEdge(internal_dst_id, distance);
}

void RoutingTopology::deleteEdge(size_t internal_from, size_t internal_to) {
    m_topo[internal_from].deleteEdge(internal_to);
}

//////////////////////////////////////////////////////////////////////

}  // namespace florasat
