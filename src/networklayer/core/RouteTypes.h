/*
 * RouteTypes.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include <omnetpp.h>

#include <optional>
#include <span>
#include <vector>

#include "core/macros/Slice.h"
#include "core/macros/Validate.h"

namespace florasat {

class IRoute {
   public:
    virtual ~IRoute() = default;
    virtual std::vector<size_t> const& getFullPath() const = 0;
    virtual size_t getNode(size_t index) const = 0;
    virtual size_t getNumberOfNodes() const = 0;
    virtual size_t getNumberOfHops() const = 0;
    virtual std::span<size_t> const getSatellitePath() = 0;
    virtual size_t getNumberOfSatelliteHops() const = 0;
};

class BasePath : public IRoute {
   public:
    BasePath() {}
    BasePath(std::vector<size_t> path) {
        m_path = std::move(path);
        VALIDATE2(m_path.size() > 0, "Path should contain at least one node!");
    }
    virtual ~BasePath() = default;
    std::vector<size_t> const& getFullPath() const override;
    size_t getNode(size_t index) const override;
    size_t getNumberOfNodes() const override;
    size_t getNumberOfHops() const override;

   protected:
    std::vector<size_t> m_path;
};

class G2GPath : public BasePath {
   public:
    G2GPath(std::vector<size_t> path) : BasePath(path) {
        VALIDATE(path.size() >= 3);
    };
    virtual ~G2GPath() = default;

    virtual std::span<size_t> const getSatellitePath() override;
    virtual size_t getNumberOfSatelliteHops() const override;
    virtual size_t getAccessSatellite() const;
    virtual size_t getLastSatellite() const;

    size_t getSrcGsId() const;
    size_t getDstGsId() const;
};

class S2SPath : public BasePath {
   public:
    S2SPath(std::vector<size_t> path) : BasePath(path) {
        VALIDATE(path.size() >= 1);
    };
    virtual ~S2SPath() = default;

    virtual std::span<size_t> const getSatellitePath() override;
    virtual size_t getNumberOfSatelliteHops() const override;

    size_t getSrcSatId() const;
    size_t getDstSatId() const;

   protected:
    std::vector<size_t> m_path;
};

class S2GPath : public BasePath {
   public:
    S2GPath(std::vector<size_t> path) : BasePath(path) {
        VALIDATE(path.size() >= 2);
    };
    virtual ~S2GPath() = default;

    virtual std::span<size_t> const getSatellitePath() override;
    virtual size_t getNumberOfSatelliteHops() const override;

    size_t getSrcSatId() const;
    size_t getLastSatellite() const;
    size_t getDstGsId() const;

   protected:
    std::vector<size_t> m_path;
};

}  // namespace florasat