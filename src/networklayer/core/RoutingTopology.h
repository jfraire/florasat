/*
 * RoutingTopology.h
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include <algorithm>
#include <optional>
#include <vector>

#include "core/mobility/libnorad/cVector.h"
#include "core/mobility/libnorad/globals.h"

namespace florasat {

using namespace omnetpp;

enum class NodeType {
    GS,
    SAT
};

class Edge {
   public:
    Edge(size_t to, double distance);
    size_t getTo() const;
    double getDistance() const;
    void updateDistance(double newDistance);

   private:
    Edge(){};

   protected:
    size_t m_to;
    double m_distance;
};

class Node {
   public:
    Node(NodeType type, cVector position);
    Node(NodeType type, double x, double y, double z);

    const cVector& getPosition() const;
    void updatePosition(double x, double y, double z);

    const std::vector<Edge>& getEdges() const;
    bool isGS() const;
    bool isSat() const;

    std::vector<Edge>& getEdgesForUpdate();
    void addEdge(size_t to, double distance);
    void deleteEdge(size_t to);

   private:
    Node(){};

   protected:
    NodeType m_type;
    cVector m_position;
    std::vector<Edge> m_edges;
};

class RoutingTopology {
   public:
    RoutingTopology(std::vector<Node> satellites, std::vector<Node> groundstations);

    void connectISL(size_t first_sat_id, size_t second_sat_id, double distance);
    void disconnectISL(size_t first_sat_id, size_t second_sat_id);

    void connectGSL(size_t gs_id, size_t sat_id, double distance);
    void disconnectGSL(size_t gs_id, size_t sat_id);

    void updatePosition(size_t sat_id, double x, double y, double z);

    const std::vector<Edge>& getEdges(size_t internal_id) const;

    size_t calculateInternalSatId(size_t sat_id) const;
    size_t calculateInternalGsId(size_t gs_id) const;
    size_t calculateExternalSatId(size_t internal_sat_id) const;
    size_t calculateExternalGsId(size_t internal_gs_id) const;

    bool isSatellite(size_t internal_id) const;
    bool isGroundstation(size_t internal_id) const;

    size_t size() const;
    void print() const;

    double calculateSatsDistance(size_t first_sat_id, size_t second_sat_id) const;
    double calculateSatGsDistance(size_t sat_id, size_t gs_id) const;
    double calculateElevation(size_t sat_id, size_t gs_id) const;

   protected:
    std::optional<double> distanceInternal(size_t first, size_t second);
    void addOrUpdateEdge(size_t internal_from, size_t internal_to, double distance);
    void deleteEdge(size_t internal_from, size_t internal_to);

   protected:
    size_t m_sat_count;
    size_t m_gs_count;
    std::vector<Node> m_topo;
};

}  // namespace florasat
