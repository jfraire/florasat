/*
 * Dijkstra.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include <omnetpp.h>

#include <vector>

#include "RouteTypes.h"
#include "RoutingTopology.h"
#include "core/common/PriorityQueue.h"
#include "orbit/constellation/topologycontrol/TopologyControlBase.h"

using namespace omnetpp;

namespace florasat {
namespace dijkstra {

std::tuple<G2GPath, double> generateG2GPath(RoutingTopology const& topology, size_t gs_id_start, size_t gs_id_end);
std::tuple<S2SPath, double> generateS2SPath(RoutingTopology const& topology, size_t sat_id_start, size_t sat_id_end);
// std::vector<size_t> generateSatGsPath(RoutingTopology const& topology, size_t sat_id_start, size_t gs_id_end);
// std::vector<size_t> generateGsSatPath(RoutingTopology const& topology, size_t gs_id_start, size_t sat_id_end);
// std::vector<size_t> generateSatSatPath(RoutingTopology const& topology, size_t sat_id_start, size_t sat_id_end);

}  // namespace dijkstra
}  // namespace florasat
