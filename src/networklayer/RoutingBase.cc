/*
 * RoutingBase.cc
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#include "RoutingBase.h"

namespace florasat {

Define_Module(RoutingBase);

RoutingBase::~RoutingBase() {
}

void RoutingBase::initialize(int stage) {
    cModule::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        topology_control = check_and_cast<TopologyControlBase*>(getSystemModule()->getSubmodule("topology_control"));
        ground_station = check_and_cast<GroundStation*>(getParentModule());
        gs_id = getParentModule()->getIndex();
    } else if (stage == inet::INITSTAGE_LAST) {
        // validate that routing algorithm supports Walker Type
        auto supports = getSupportedConstellationType();
        auto type = topology_control->getConstellationType();
        if (type == ConstellationType::WALKER_DELTA) {
            VALIDATE2(supports == SupportedConstellationType::DELTA || supports == SupportedConstellationType::BOTH, "Choosen routing algorithm does not support WALKER-DELTA constellations.");
        } else if (type == ConstellationType::WALKER_STAR) {
            VALIDATE2(supports == SupportedConstellationType::STAR || supports == SupportedConstellationType::BOTH, "Choosen routing algorithm does not support WALKER-STAR constellations.");
        }
    }
}

G2GPath RoutingBase::calculate_route(size_t dst_gs_id) {
    Enter_Method("calculate_route");
    RoutingTopology const& topology = topology_control->get_topology();
    auto [path, distance] = dijkstra::generateG2GPath(topology, gs_id, dst_gs_id);
    return path;
}

// bool validate_route(std::vector<size_t> const& route, RoutingTopology const& topology, std::vector<size_t> const& firstSats, std::vector<size_t> const& secondSats) {
//     bool firstSatReachable = contains(firstSats, route[0]);
//     bool routeValid = true;
//     for (size_t i = 0; i < route.size(); i++) {
//         bool isLast = i == route.size() - 1;
//         if (isLast) continue;

//         size_t from = route[i];
//         size_t to = route[i + 1];

//         auto distance_opt = (*topology)[from][to];

//         if (!distance_opt.has_value()) {
//             routeValid = false;
//             break;
//         }
//     }

//     bool lastSatReachable = contains(secondSats, route[route.size() - 1]);
//     return firstSatReachable && routeValid && lastSatReachable;
// }

}  // namespace florasat
