/*
 * CstlRoutingTable.cc
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#include "CstlRoutingTable.h"

using namespace omnetpp;

namespace florasat {

Define_Module(CstlRoutingTable);

CstlRoutingTable::~CstlRoutingTable() {
}

void CstlRoutingTable::initialize(int stage) {
    cSimpleModule::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        int numGs = getSystemModule()->getSubmoduleVectorSize("groundstation");
        for (size_t i = 0; i < numGs; i++) {
            cModule* gs = getSystemModule()->getSubmodule("groundstation", i);
            int numApps = gs->getSubmoduleVectorSize("app");
            for (size_t a = 0; a < numApps; a++) {
                std::string address_raw = "";
                auto app = gs->getSubmodule("app", a);

                if (app->hasPar("localAddress")) {
                    address_raw = app->par("localAddress").stdstringValue();
                } else if (app->hasSubmodule("io")) {
                    address_raw = app->getSubmodule("io")->par("localAddress").stdstringValue();
                } else if (app->hasSubmodule("listener")) {
                    address_raw = app->getSubmodule("listener")->par("localAddress").stdstringValue();
                }

                VALIDATE2(!address_raw.empty(), "Could not load address for app.");

                // std::cout << address_raw << std::endl;
                auto addr = L3Address(address_raw.c_str());
                addEntry(addr, i);
            }
        }

    } else if (stage == inet::INITSTAGE_NETWORK_LAYER) {
    }
}

void CstlRoutingTable::addEntry(L3Address address, size_t gsId) {
    if (addresses.find(address) != addresses.end()) {
        error("Error in ConstellationRoutingTable::addEntry: Entry for %s already present.", address.str().c_str());
    }
    addresses.emplace(address, gsId);
}

void CstlRoutingTable::removeEntry(L3Address address, size_t gsId) {
    if (addresses.find(address) == addresses.end()) {
        error("Error in ConstellationRoutingTable::addEntry: Entry for %s not present.", address.str().c_str());
    }
    addresses.erase(address);
}

std::optional<L3Address> CstlRoutingTable::getAddressOfGroundStation(size_t gsId) {
    for (auto t : addresses) {
        if (t.second == gsId) {
            return t.first;
        }
    }
    return std::nullopt;
}

std::optional<size_t> CstlRoutingTable::getGroundStationForAddress(L3Address address) {
    if (addresses.find(address) == addresses.end()) {
        return std::nullopt;
    } else {
        return {addresses.at(address)};
    }
}

}  // namespace florasat
