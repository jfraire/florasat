/*
 * CstlRoutingTable.h
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include <optional>

#include "core/macros/Validate.h"
#include "inet/common/INETDefs_m.h"
#include "inet/networklayer/common/L3Address.h"

using namespace omnetpp;
using namespace inet;

namespace florasat {

class CstlRoutingTable : public cSimpleModule {
   public:
    CstlRoutingTable() {}
    virtual ~CstlRoutingTable();

    void addEntry(L3Address address, size_t gsId);
    void removeEntry(L3Address address, size_t gsId);
    std::optional<L3Address> getAddressOfGroundStation(size_t gsId);
    std::optional<size_t> getGroundStationForAddress(L3Address address);

   protected:
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

   protected:
    std::map<L3Address, size_t> addresses;
};

}  // namespace florasat
