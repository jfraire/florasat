/*
 * RoutingBase.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include <omnetpp.h>

#include "core/Dijkstra.h"
#include "core/SupportedConstellationType.h"
#include "inet/common/stlutils.h"
#include "orbit/constellation/topologycontrol/TopologyControlBase.h"

using namespace omnetpp;

namespace florasat {

class RoutingBase : public cSimpleModule {
   public:
    RoutingBase() {}
    virtual ~RoutingBase();

    virtual G2GPath calculate_route(size_t dst_gs_id);

   protected:
    virtual SupportedConstellationType getSupportedConstellationType() const { return SupportedConstellationType::BOTH; }
    virtual std::string getAlgorithmName() const { return std::string("Dijkstra"); }
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

   protected:
    TopologyControlBase* topology_control = nullptr;
    GroundStation* ground_station = nullptr;

    size_t gs_id;
};

}  // namespace florasat
