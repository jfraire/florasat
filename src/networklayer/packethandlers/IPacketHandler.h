/*
 * IPacketHandler.h
 *
 *  Created on: Jan 05, 2023
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include "inet/common/Simsignals_m.h"
#include "inet/common/packet/Packet.h"

using namespace omnetpp;

namespace florasat {

class IPacketHandler {
   public:
    virtual void handlePacket(inet::Packet *packet) = 0;
};

}  // namespace florasat
