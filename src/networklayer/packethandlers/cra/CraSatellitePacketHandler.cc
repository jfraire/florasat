/*
 * CraSatellitePacketHandler.cc
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#include "CraSatellitePacketHandler.h"

using namespace omnetpp;

namespace florasat {

Define_Module(CraSatellitePacketHandler);

CraSatellitePacketHandler::~CraSatellitePacketHandler() {
}

void CraSatellitePacketHandler::initialize(int stage) {
    cModule::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        topology_control = check_and_cast<TopologyControlBase *>(getSystemModule()->getSubmodule("topology_control"));
        satellite = check_and_cast<Satellite *>(getParentModule());
    } else if (stage == inet::INITSTAGE_NETWORK_LAYER) {
        // registerService(Protocol::ipv4, gate("transportIn"), gate("transportOut"));
        // registerProtocol(Protocol::ipv4, gate("queueOut"), gate("queueIn"));
    }
}

// void CraSatellitePacketHandler::handleRegisterService(const Protocol &protocol, cGate *gate, ServicePrimitive servicePrimitive) {
//     Enter_Method("handleRegisterService");
// }

// void CraSatellitePacketHandler::handleRegisterProtocol(const Protocol &protocol, cGate *gate, ServicePrimitive servicePrimitive) {
//     Enter_Method("handleRegisterProtocol");
// }

void CraSatellitePacketHandler::handlePacket(inet::Packet *packet) {
    Enter_Method("handlePacket");
    take(packet);

    // increase hopcount
    auto metadata_tag = packet->getTagForUpdate<MetadataTag>();
    metadata_tag->setHops(metadata_tag->getHops() + 1);

    // EV << "Handling " << packet << endl;
    auto cstlSrcRoutingHeader = packet->removeAtFront<CstlSourceRoutingHeader>();

    // update ipv4 header
    auto ipv4Header = packet->removeAtFront<Ipv4Header>();
    // Ipv4Address srcAddr = ipv4Header->getSrcAddress();
    // Ipv4Address destAddr = ipv4Header->getDestAddress();
    auto ttl = ipv4Header->getTimeToLive();
    // EV << "Rcvd packet from " << srcAddr << " to " << destAddr << "; TTL: " << ttl << endl;

    // hop counter check
    if (ttl <= 0) {
        // drop datagram, destruction responsibility in ICMP
        PacketDropDetails details;
        details.setReason(HOP_LIMIT_REACHED);
        emit(packetDroppedSignal, packet, &details);
        numDropped++;
        delete packet;
        return;
    }
    ipv4Header->setTimeToLive(ttl - 1);
    insertNetworkProtocolHeader(packet, Protocol::ipv4, ipv4Header);

    // cstl forwarding
    // check for ground link required
    if (cstlSrcRoutingHeader->getSatelliteArraySize() <= 1) {
        // no more satellite hops
        auto gsl_gate_opt = satellite->getGateToPartner(DynamicGateType::GSL, cstlSrcRoutingHeader->getDstGs());
        if (gsl_gate_opt.has_value()) {
            // connection exists
            packet->insertAtFront(cstlSrcRoutingHeader);
            sendDirect(packet, gsl_gate_opt.value());
        } else {
            // no connection to that ground station
            EV << "Channel to ground station " << cstlSrcRoutingHeader->getDstGs() << " not available. Dropping...";
            PacketDropDetails details;
            details.setReason(PacketDropReason::INTERFACE_DOWN);
            emit(packetDroppedSignal, packet, &details);
            numDropped++;
            delete packet;
        }
        return;
    }

    cstlSrcRoutingHeader->eraseSatellite(0);
    size_t nextHop = cstlSrcRoutingHeader->getSatellite(0);
    cstlSrcRoutingHeader->addChunkLength(-B(2));
    auto isl_gate_opt = satellite->getGateToPartner(DynamicGateType::ISL, nextHop);
    if (isl_gate_opt.has_value()) {
        // connection exists
        packet->insertAtFront(cstlSrcRoutingHeader);
        sendDirect(packet, isl_gate_opt.value());
    } else {
        // no connection to that satellite
        EV << "ISL Channel to satellite " << nextHop << " not available. Dropping...";
        PacketDropDetails details;
        details.setReason(PacketDropReason::INTERFACE_DOWN);
        emit(packetDroppedSignal, packet, &details);
        numDropped++;
        delete packet;
    }
}

void CraSatellitePacketHandler::refreshDisplay() const {
    char buf[80] = "";
    // if (numForwarded > 0)
    //     sprintf(buf + strlen(buf), "fwd:%d ", numForwarded);
    // if (numLocalDeliver > 0)
    //     sprintf(buf + strlen(buf), "up:%d ", numLocalDeliver);
    // if (numMulticast > 0)
    //     sprintf(buf + strlen(buf), "mcast:%d ", numMulticast);
    if (numDropped > 0)
        sprintf(buf + strlen(buf), "DROP:%d ", numDropped);
    if (numUnroutable > 0)
        sprintf(buf + strlen(buf), "UNROUTABLE:%d ", numUnroutable);
    getDisplayString().setTagArg("t", 0, buf);
}

// void CraSatellitePacketHandler::sendMessageToSat(inet::Packet *packet, size_t sat_id) {
//     EV << "Send " << packet << " to sat " << sat_id << endl;
//     delete packet;
// }
// void CraSatellitePacketHandler::sendMessageToGS(inet::Packet *packet, size_t gs_id) {
//     EV << "Send " << packet << " to gs " << gs_id << endl;
//     delete packet;
// }
// void CraSatellitePacketHandler::dropPacket(inet::Packet *packet, inet::PacketDropReason reason) {
//     EV << "Drop " << packet << " due to " << std::to_string(reason) << endl;
//     delete packet;
// }

}  // namespace florasat
