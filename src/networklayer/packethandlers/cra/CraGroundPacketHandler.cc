/*
 * CraGroundPacketHandler.cc
 *
 * Created on: Jan 08, 2024
 *     Author: Robin Ohs
 */

#include "CraGroundPacketHandler.h"

using namespace omnetpp;

namespace florasat {

Define_Module(CraGroundPacketHandler);

CraGroundPacketHandler::~CraGroundPacketHandler() {
    for (auto it : socketIdToSocketDescriptor)
        delete it.second;
}

void CraGroundPacketHandler::initialize(int stage) {
    OperationalBase::initialize(stage);

    if (stage == inet::INITSTAGE_LOCAL) {
        ground_station = check_and_cast<GroundStation *>(getParentModule());
        cstl_routing_table = check_and_cast<CstlRoutingTable *>(getSystemModule()->getSubmodule("cstl_routing_table"));
        routing = check_and_cast<RoutingBase *>(getParentModule()->getSubmodule("routing"));

        const char *crcModeString = par("crcMode");
        crcMode = parseCrcMode(crcModeString, false);

        defaultTimeToLive = par("timeToLive");
        defaultMCTimeToLive = par("multicastTimeToLive");
    } else if (stage == inet::INITSTAGE_NETWORK_LAYER) {
        registerService(Protocol::ipv4, gate("transportIn"), gate("transportOut"));
        registerProtocol(Protocol::ipv4, gate("queueOut"), gate("queueIn"));
    }
}

void CraGroundPacketHandler::handleRegisterService(const Protocol &protocol, cGate *gate, ServicePrimitive servicePrimitive) {
    Enter_Method("handleRegisterService");
}

void CraGroundPacketHandler::handleRegisterProtocol(const Protocol &protocol, cGate *gate, ServicePrimitive servicePrimitive) {
    Enter_Method("handleRegisterProtocol");
    if (gate->isName("transportOut"))
        upperProtocols.insert(&protocol);
}

void CraGroundPacketHandler::handleMessageWhenUp(cMessage *msg) {
    if (msg->arrivedOn("transportIn")) {
        EV_INFO << "Received " << msg << " from transport.\n";
        if (auto request = dynamic_cast<Request *>(msg))
            handleRequest(request);
        else
            handlePacketFromHL(check_and_cast<Packet *>(msg));
    } else if (msg->arrivedOn("queueIn")) {  // from network
        EV_INFO << "Received " << msg << " from network.\n";
        handleIncomingDatagram(check_and_cast<Packet *>(msg));
    } else
        throw cRuntimeError("message arrived on unknown gate '%s'", msg->getArrivalGate()->getName());
}

void CraGroundPacketHandler::handleRequest(Request *request) {
    auto ctrl = request->getControlInfo();
    if (ctrl == nullptr)
        throw cRuntimeError("Request '%s' arrived without controlinfo", request->getName());
    else if (Ipv4SocketBindCommand *command = dynamic_cast<Ipv4SocketBindCommand *>(ctrl)) {
        int socketId = request->getTag<SocketReq>()->getSocketId();
        SocketDescriptor *descriptor = new SocketDescriptor(socketId, command->getProtocol() ? command->getProtocol()->getId() : -1, command->getLocalAddress());
        socketIdToSocketDescriptor[socketId] = descriptor;
        delete request;
    } else if (Ipv4SocketConnectCommand *command = dynamic_cast<Ipv4SocketConnectCommand *>(ctrl)) {
        int socketId = request->getTag<SocketReq>()->getSocketId();
        if (!containsKey(socketIdToSocketDescriptor, socketId))
            throw cRuntimeError("Ipv4Socket: should use bind() before connect()");
        socketIdToSocketDescriptor[socketId]->remoteAddress = command->getRemoteAddress();
        delete request;
    } else if (dynamic_cast<Ipv4SocketCloseCommand *>(ctrl) != nullptr) {
        int socketId = request->getTag<SocketReq>()->getSocketId();
        auto it = socketIdToSocketDescriptor.find(socketId);
        if (it != socketIdToSocketDescriptor.end()) {
            delete it->second;
            socketIdToSocketDescriptor.erase(it);
            auto indication = new Indication("closed", IPv4_I_SOCKET_CLOSED);
            auto ctrl = new Ipv4SocketClosedIndication();
            indication->setControlInfo(ctrl);
            indication->addTag<SocketInd>()->setSocketId(socketId);
            send(indication, "transportOut");
        }
        delete request;
    } else if (dynamic_cast<Ipv4SocketDestroyCommand *>(ctrl) != nullptr) {
        int socketId = request->getTag<SocketReq>()->getSocketId();
        auto it = socketIdToSocketDescriptor.find(socketId);
        if (it != socketIdToSocketDescriptor.end()) {
            delete it->second;
            socketIdToSocketDescriptor.erase(it);
        }
        delete request;
    } else
        throw cRuntimeError("Unknown command: '%s' with %s", request->getName(), ctrl->getClassName());
}

void CraGroundPacketHandler::handlePacketFromHL(inet::Packet *packet) {
    emit(packetReceivedFromUpperSignal, packet);

    auto socketReq = packet->findTag<SocketReq>();
    if (socketReq != nullptr) {
        int socketId = socketReq->getSocketId();
        auto it = socketIdToSocketDescriptor.find(socketId);
        if (it != socketIdToSocketDescriptor.end()) {
            auto descriptor = it->second;
            if (!packet->hasTag<L3AddressReq>())
                packet->addTag<L3AddressReq>()->setDestAddress(descriptor->remoteAddress);
        }
    }

    // encapsulate
    encapsulate(packet);

    // send to satellite
    datagramLocalOut(packet);
}

void CraGroundPacketHandler::datagramLocalOut(Packet *packet) {
    const auto &ipv4Header = packet->peekAtFront<Ipv4Header>();

    // get IDs for src and dst ground station
    size_t srcGs = ground_station->getIndex();
    auto dstGs_opt = cstl_routing_table->getGroundStationForAddress(ipv4Header->getDestAddress());
    ASSERT(dstGs_opt.has_value());
    size_t dstGs = dstGs_opt.value();
    if (ground_station->getPartners(DynamicGateType::GSL).size() == 0) {
        numUnroutable++;
        PacketDropDetails details;
        details.setReason(NO_ROUTE_FOUND);
        emit(packetDroppedSignal, packet, &details);
        delete packet;
        return;
    }

    // calculate route
    G2GPath route = routing->calculate_route(dstGs);

    // Create source routing header and insert route
    auto sourceRoutingHeader = makeShared<CstlSourceRoutingHeader>();
    sourceRoutingHeader->setSrcGs(srcGs);
    sourceRoutingHeader->setDstGs(dstGs);
    auto sat_path = route.getSatellitePath();
    sourceRoutingHeader->setSatelliteArraySize(sat_path.size());
    int i = 0;
    for (size_t sat : sat_path) {
        sourceRoutingHeader->setSatellite(i, sat);
        sourceRoutingHeader->addChunkLength(B(2));
        i++;
    }
    packet->insertAtFront(sourceRoutingHeader);

    // add metadata tags
    packet->addTag<CreationTimeTag>();
    auto metadata_tag = packet->addTag<MetadataTag>();
    metadata_tag->setSrcGs(srcGs);
    metadata_tag->setDstGs(dstGs);

    // forward packet to access satellite
    size_t access_satellite = route.getAccessSatellite();
    auto gate_opt = ground_station->getGateToPartner(DynamicGateType::GSL, access_satellite);
    if (!gate_opt.has_value()) {
        EV << "Channel to first satellite " << access_satellite << " not available. Dropping...";
        numUnroutable++;
        PacketDropDetails details;
        details.setReason(PacketDropReason::INTERFACE_DOWN);
        emit(packetDroppedSignal, packet, &details);
        delete packet;
        return;
    }
    // send packet
    auto gate = gate_opt.value();
    sendDirect(packet, gate);
}

void CraGroundPacketHandler::encapsulate(Packet *transportPacket) {
    const auto &ipv4Header = makeShared<Ipv4Header>();

    auto l3AddressReq = transportPacket->removeTag<L3AddressReq>();
    Ipv4Address src = l3AddressReq->getSrcAddress().toIpv4();
    // bool nonLocalSrcAddress = l3AddressReq->getNonLocalSrcAddress();
    Ipv4Address dest = l3AddressReq->getDestAddress().toIpv4();

    ipv4Header->setProtocolId((IpProtocolId)ProtocolGroup::getIpProtocolGroup()->getProtocolNumber(transportPacket->getTag<PacketProtocolTag>()->getProtocol()));

    auto hopLimitReq = transportPacket->removeTagIfPresent<HopLimitReq>();
    short ttl = (hopLimitReq != nullptr) ? hopLimitReq->getHopLimit() : -1;
    bool dontFragment = false;
    if (auto &dontFragmentReq = transportPacket->removeTagIfPresent<FragmentationReq>())
        dontFragment = dontFragmentReq->getDontFragment();

    // set source and destination address
    ipv4Header->setDestAddress(dest);

    // when source address was given, use it; otherwise it'll get the address
    // of the outgoing interface after routing
    if (!src.isUnspecified()) {
        ipv4Header->setSrcAddress(src);
    }

    // set other fields
    if (auto &tosReq = transportPacket->removeTagIfPresent<TosReq>()) {
        ipv4Header->setTypeOfService(tosReq->getTos());
        if (transportPacket->findTag<DscpReq>())
            throw cRuntimeError("TosReq and DscpReq found together");
        if (transportPacket->findTag<EcnReq>())
            throw cRuntimeError("TosReq and EcnReq found together");
        transportPacket->addTag<TosInd>()->setTos(ipv4Header->getTypeOfService());
    }
    if (auto &dscpReq = transportPacket->removeTagIfPresent<DscpReq>()) {
        ipv4Header->setDscp(dscpReq->getDifferentiatedServicesCodePoint());
        transportPacket->addTag<DscpInd>()->setDifferentiatedServicesCodePoint(ipv4Header->getDscp());
    }
    if (auto &ecnReq = transportPacket->removeTagIfPresent<EcnReq>()) {
        ipv4Header->setEcn(ecnReq->getExplicitCongestionNotification());
        transportPacket->addTag<EcnInd>()->setExplicitCongestionNotification(ipv4Header->getEcn());
    }

    ipv4Header->setIdentification(curFragmentId++);
    ipv4Header->setMoreFragments(false);
    ipv4Header->setDontFragment(dontFragment);
    ipv4Header->setFragmentOffset(0);

    if (ttl != -1) {
        ASSERT(ttl > 0);
    } else if (ipv4Header->getDestAddress().isLinkLocalMulticast())
        ttl = 1;
    else if (ipv4Header->getDestAddress().isMulticast())
        ttl = defaultMCTimeToLive;
    else
        ttl = defaultTimeToLive;
    ipv4Header->setTimeToLive(ttl);

    if (auto &optReq = transportPacket->removeTagIfPresent<Ipv4OptionsReq>()) {
        for (size_t i = 0; i < optReq->getOptionArraySize(); i++) {
            auto opt = optReq->removeOption(i);
            ipv4Header->addOption(opt);
            ipv4Header->addChunkLength(B(opt->getLength()));
        }
    }

    ASSERT(ipv4Header->getChunkLength() <= IPv4_MAX_HEADER_LENGTH);
    ipv4Header->setHeaderLength(ipv4Header->getChunkLength());
    ipv4Header->setTotalLengthField(ipv4Header->getChunkLength() + transportPacket->getDataLength());
    ipv4Header->setCrcMode(crcMode);
    ipv4Header->setCrc(0);
    switch (crcMode) {
        case CRC_DECLARED_CORRECT:
            // if the CRC mode is declared to be correct, then set the CRC to an easily recognizable value
            ipv4Header->setCrc(0xC00D);
            break;
        case CRC_DECLARED_INCORRECT:
            // if the CRC mode is declared to be incorrect, then set the CRC to an easily recognizable value
            ipv4Header->setCrc(0xBAAD);
            break;
        case CRC_COMPUTED: {
            ipv4Header->setCrc(0);
            // crc will be calculated in fragmentAndSend()
            break;
        }
        default:
            throw cRuntimeError("Unknown CRC mode");
    }
    insertNetworkProtocolHeader(transportPacket, Protocol::ipv4, ipv4Header);
    // setting Ipv4 options is currently not supported
}

void CraGroundPacketHandler::refreshDisplay() const {
    OperationalBase::refreshDisplay();

    char buf[80] = "";
    // if (numForwarded > 0)
    //     sprintf(buf + strlen(buf), "fwd:%d ", numForwarded);
    // if (numLocalDeliver > 0)
    //     sprintf(buf + strlen(buf), "up:%d ", numLocalDeliver);
    // if (numMulticast > 0)
    //     sprintf(buf + strlen(buf), "mcast:%d ", numMulticast);
    if (numDropped > 0)
        sprintf(buf + strlen(buf), "DROP:%d ", numDropped);
    if (numUnroutable > 0)
        sprintf(buf + strlen(buf), "UNROUTABLE:%d ", numUnroutable);
    getDisplayString().setTagArg("t", 0, buf);
}

void CraGroundPacketHandler::handleStartOperation(LifecycleOperation *operation) {
}

void CraGroundPacketHandler::handleStopOperation(LifecycleOperation *operation) {
    for (auto it : socketIdToSocketDescriptor)
        delete it.second;
    socketIdToSocketDescriptor.clear();
}

void CraGroundPacketHandler::handleCrashOperation(LifecycleOperation *operation) {
    for (auto it : socketIdToSocketDescriptor)
        delete it.second;
    socketIdToSocketDescriptor.clear();
}

void CraGroundPacketHandler::handleIncomingDatagram(Packet *packet) {
    ASSERT(packet);
    auto cstlSourceRoutingHeader = packet->removeAtFront<CstlSourceRoutingHeader>();
    if (cstlSourceRoutingHeader->getDstGs() != ground_station->getIndex()) {
        // wrong delivered
        EV_WARN << "Packet was send to wrong destination.\n";
        PacketDropDetails details;
        details.setReason(NOT_ADDRESSED_TO_US);
        emit(packetDroppedSignal, packet, &details);
        delete packet;
        return;
    }

    // increase hopcount
    auto metadata_tag = packet->getTagForUpdate<MetadataTag>();
    metadata_tag->setHops(metadata_tag->getHops() + 1);

    emit(packetReceivedFromLowerSignal, packet);

    const auto &ipv4Header = packet->peekAtFront<Ipv4Header>();
    packet->addTagIfAbsent<NetworkProtocolInd>()->setProtocol(&Protocol::ipv4);
    packet->addTagIfAbsent<NetworkProtocolInd>()->setNetworkProtocolHeader(ipv4Header);

    if (!ipv4Header->isCorrect() && !ipv4Header->verifyCrc()) {
        EV_WARN << "CRC error found, drop packet\n";
        PacketDropDetails details;
        details.setReason(INCORRECTLY_RECEIVED);
        emit(packetDroppedSignal, packet, &details);
        delete packet;
        return;
    }

    if (ipv4Header->getTotalLengthField() > packet->getDataLength()) {
        EV_WARN << "length error found, sending ICMP_PARAMETER_PROBLEM\n";
        // sendIcmpError(packet, interfaceId, ICMP_PARAMETER_PROBLEM, 0);
        return;
    }

    // remove lower layer paddings:
    if (ipv4Header->getTotalLengthField() < packet->getDataLength()) {
        packet->setBackOffset(packet->getFrontOffset() + ipv4Header->getTotalLengthField());
    }

    // check for header biterror
    if (packet->hasBitError()) {
        // probability of bit error in header = size of header / size of total message
        // (ignore bit error if in payload)
        double relativeHeaderLength = B(ipv4Header->getHeaderLength()).get() / (double)B(ipv4Header->getChunkLength()).get();
        if (dblrand() <= relativeHeaderLength) {
            EV_WARN << "bit error found, sending ICMP_PARAMETER_PROBLEM\n";
            // sendIcmpError(packet, interfaceId, ICMP_PARAMETER_PROBLEM, 0);
            return;
        }
    }

    EV_DETAIL << "Received datagram `" << ipv4Header->getName() << "' with dest=" << ipv4Header->getDestAddress() << "\n";

    EV_INFO << "Delivering " << packet << " locally.\n";
    if (ipv4Header->getSrcAddress().isUnspecified())
        EV_WARN << "Received datagram '" << packet->getName() << "' without source address filled in\n";

    const Protocol *protocol = ipv4Header->getProtocol();
    auto remoteAddress(ipv4Header->getSrcAddress());
    auto localAddress(ipv4Header->getDestAddress());
    decapsulate(packet);

    if (contains(upperProtocols, protocol)) {
        EV_INFO << "Passing up to protocol " << *protocol << "\n";
        emit(packetSentToUpperSignal, packet);
        send(packet, "transportOut");
        numLocalDeliver++;
    } else {
        EV_ERROR << "Transport protocol '" << protocol->getName() << "' not connected, discarding packet\n";
        // packet->setFrontOffset(ipv4HeaderPosition);
        // // get source interface:
        // const auto &tag = packet->findTag<InterfaceInd>();
        // sendIcmpError(packet, tag ? tag->getInterfaceId() : -1, ICMP_DESTINATION_UNREACHABLE, ICMP_DU_PROTOCOL_UNREACHABLE);

        PacketDropDetails details;
        details.setReason(PacketDropReason::NO_PROTOCOL_FOUND);
        emit(packetDroppedSignal, packet, &details);
        delete packet;
    }
}

void CraGroundPacketHandler::decapsulate(Packet *packet) {
    // decapsulate transport packet
    const auto &ipv4Header = packet->popAtFront<Ipv4Header>();

    // create and fill in control info
    packet->addTagIfAbsent<DscpInd>()->setDifferentiatedServicesCodePoint(ipv4Header->getDscp());
    packet->addTagIfAbsent<EcnInd>()->setExplicitCongestionNotification(ipv4Header->getEcn());
    packet->addTagIfAbsent<TosInd>()->setTos(ipv4Header->getTypeOfService());

    auto transportProtocol = ProtocolGroup::getIpProtocolGroup()->getProtocol(ipv4Header->getProtocolId());
    packet->addTagIfAbsent<PacketProtocolTag>()->setProtocol(transportProtocol);
    packet->addTagIfAbsent<DispatchProtocolReq>()->setProtocol(transportProtocol);
    auto l3AddressInd = packet->addTagIfAbsent<L3AddressInd>();
    l3AddressInd->setSrcAddress(ipv4Header->getSrcAddress());
    l3AddressInd->setDestAddress(ipv4Header->getDestAddress());
    packet->addTagIfAbsent<HopLimitInd>()->setHopLimit(ipv4Header->getTimeToLive());
}

}  // namespace florasat
