/*
 * CraSatellitePacketHandler.h
 *
 * Author: Robin Ohs
 * Version: 1.0
 */

#pragma once

#include <omnetpp.h>

#include <string>

#include "networklayer/packethandlers/IPacketHandler.h"
#include "inet/common/INETDefs.h"
#include "inet/common/Simsignals.h"
#include "inet/common/packet/Message.h"
#include "inet/common/packet/Packet.h"
#include "inet/common/socket/SocketTag_m.h"
#include "inet/common/stlutils.h"
#include "inet/networklayer/base/NetworkProtocolBase.h"
#include "inet/networklayer/common/DscpTag_m.h"
#include "inet/networklayer/common/EcnTag_m.h"
#include "inet/networklayer/common/FragmentationTag_m.h"
#include "inet/networklayer/common/HopLimitTag_m.h"
#include "inet/networklayer/common/L3Address.h"
#include "inet/networklayer/common/L3AddressTag_m.h"
#include "inet/networklayer/common/L3Tools.h"
#include "inet/networklayer/common/TosTag_m.h"
#include "inet/networklayer/contract/INetworkProtocol.h"
#include "inet/networklayer/contract/ipv4/Ipv4SocketCommand_m.h"
#include "inet/networklayer/ipv4/Ipv4Header_m.h"
#include "inet/networklayer/ipv4/Ipv4OptionsTag_m.h"
#include "orbit/constellation/topologycontrol/TopologyControlBase.h"
#include "orbit/satellite/base/Satellite.h"
#include "networklayer/CstlSourceRoutingHeader_m.h"
#include "statistics/MetadataTag_m.h"

using namespace omnetpp;

namespace florasat {

// class CraSatellitePacketHandler : public OperationalBase, public INetworkProtocol, public DefaultProtocolRegistrationListener {
class CraSatellitePacketHandler : public cSimpleModule,
                                  // public IPacketSender,
                                  public IPacketHandler {
   public:
    CraSatellitePacketHandler() {}
    virtual ~CraSatellitePacketHandler();

    // virtual void handleRegisterService(const Protocol& protocol, cGate* gate, ServicePrimitive servicePrimitive) override;
    // virtual void handleRegisterProtocol(const Protocol& protocol, cGate* gate, ServicePrimitive servicePrimitive) override;

   protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    // utility: show current statistics above the icon
    virtual void refreshDisplay() const override;

    // IPacketSender
    // virtual void sendMessageToSat(inet::Packet* packet, size_t sat_id) override;
    // virtual void sendMessageToGS(inet::Packet* packet, size_t gs_id) override;
    // virtual void dropPacket(inet::Packet* packet, inet::PacketDropReason reason) override;

    // IPacketHandler
    virtual void handlePacket(inet::Packet* packet) override;

   protected:
    // statistics
    int numDropped = 0;
    int numUnroutable = 0;
    int numForwarded = 0;

    // cached_ptrs
    TopologyControlBase* topology_control = nullptr;
    Satellite* satellite = nullptr;
};

}  // namespace florasat
