/*
 * CraGroundPacketHandler.h
 *
 * Created on: Jan 08, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include "inet/common/INETDefs.h"
#include "inet/common/Simsignals.h"
#include "inet/common/packet/Message.h"
#include "inet/common/packet/Packet.h"
#include "inet/common/socket/SocketTag_m.h"
#include "inet/common/stlutils.h"
#include "inet/networklayer/base/NetworkProtocolBase.h"
#include "inet/networklayer/common/DscpTag_m.h"
#include "inet/networklayer/common/EcnTag_m.h"
#include "inet/networklayer/common/FragmentationTag_m.h"
#include "inet/networklayer/common/HopLimitTag_m.h"
#include "inet/networklayer/common/L3Address.h"
#include "inet/networklayer/common/L3AddressTag_m.h"
#include "inet/networklayer/common/L3Tools.h"
#include "inet/networklayer/common/TosTag_m.h"
#include "inet/networklayer/contract/INetworkProtocol.h"
#include "inet/networklayer/contract/ipv4/Ipv4SocketCommand_m.h"
#include "inet/networklayer/ipv4/Ipv4Header_m.h"
#include "inet/networklayer/ipv4/Ipv4OptionsTag_m.h"
#include "orbit/constellation/topologycontrol/TopologyControlBase.h"
#include "networklayer/CstlSourceRoutingHeader_m.h"
#include "networklayer/RoutingBase.h"
#include "networklayer/common/CstlRoutingTable.h"
#include "statistics/MetadataTag_m.h"

using namespace omnetpp;

namespace florasat {

class CraGroundPacketHandler : public OperationalBase, public INetworkProtocol, public DefaultProtocolRegistrationListener {
   public:
    CraGroundPacketHandler() {}
    virtual ~CraGroundPacketHandler();

    virtual void handleRegisterService(const Protocol& protocol, cGate* gate, ServicePrimitive servicePrimitive) override;
    virtual void handleRegisterProtocol(const Protocol& protocol, cGate* gate, ServicePrimitive servicePrimitive) override;

   protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    virtual void handleMessageWhenUp(cMessage* msg) override;
    void handleRequest(Request* request);
    virtual void handlePacketFromHL(inet::Packet* packet);
    void encapsulate(Packet* transportPacket);
    void datagramLocalOut(Packet* packet);

    /**
     * Handle Ipv4Header messages arriving from lower layer.
     * Decrements TTL, then invokes routePacket().
     */
    virtual void handleIncomingDatagram(Packet* packet);

    /**
     * Decapsulate packet.
     */
    virtual void decapsulate(Packet* packet);

    // utility: show current statistics above the icon
    virtual void refreshDisplay() const override;

    // lifecycle
    virtual bool isInitializeStage(int stage) const override { return stage == INITSTAGE_NETWORK_LAYER; }
    virtual bool isModuleStartStage(int stage) const override { return stage == ModuleStartOperation::STAGE_NETWORK_LAYER; }
    virtual bool isModuleStopStage(int stage) const override { return stage == ModuleStopOperation::STAGE_NETWORK_LAYER; }
    virtual void handleStartOperation(LifecycleOperation* operation) override;
    virtual void handleStopOperation(LifecycleOperation* operation) override;
    virtual void handleCrashOperation(LifecycleOperation* operation) override;

    struct SocketDescriptor {
        int socketId = -1;
        int protocolId = -1;
        Ipv4Address localAddress;
        Ipv4Address remoteAddress;

        SocketDescriptor(int socketId, int protocolId, Ipv4Address localAddress)
            : socketId(socketId), protocolId(protocolId), localAddress(localAddress) {}
    };

   protected:
    // config
    CrcMode crcMode = CRC_MODE_UNDEFINED;
    int defaultTimeToLive = -1;
    int defaultMCTimeToLive = -1;

    std::set<const Protocol*> upperProtocols;  // where to send packets after decapsulation

    // working vars
    uint16_t curFragmentId = -1;  // counter, used to assign unique fragmentIds to datagrams

    // statistics
    int numDropped = 0;
    int numUnroutable = 0;
    int numLocalDeliver = 0;

    std::map<int, SocketDescriptor*> socketIdToSocketDescriptor;

    // cached_ptrs
    GroundStation* ground_station = nullptr;
    CstlRoutingTable* cstl_routing_table = nullptr;
    RoutingBase* routing = nullptr;
};

}  // namespace florasat
