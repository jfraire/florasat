/*
 * DtnSatellitePacketHandler.cc
 *
 * Created on: Jun 16, 2023
 *     Author: Sebastian Montoya
 * Updated on: Jan 08, 2024
 *     Author: Robin Ohs
 */

#include "DtnSatellitePacketHandler.h"

namespace florasat {

Define_Module(DtnSatellitePacketHandler);

void DtnSatellitePacketHandler::initialize(int stage) {
    if (stage == inet::INITSTAGE_APPLICATION_LAYER) {
        this->eid_ = getSystemModule()->getSubmoduleVectorSize("groundStation") + this->getParentModule()->getIndex() + 1;
        INorad *noradModule = check_and_cast<INorad *>(getParentModule()->getSubmodule("NoradModule"));
        if (NoradA *noradAModule = dynamic_cast<NoradA *>(noradModule)) {
            satIndex = noradAModule->getSatelliteNumber();
        }
        // Store packetLoss probability
        packetLoss_ = par("packetLoss").doubleValue();
        contactTopology_ = check_and_cast<ContactPlan *>(getSystemModule()->getSubmodule("contactPlan"));
        topology_control = check_and_cast<DtnTopologyControl *>(getSystemModule()->getSubmodule("topology_control"));
    }
}

void DtnSatellitePacketHandler::handleMessage(cMessage *msg)
{
    if (msg->getKind() == BUNDLE || msg->getKind() == BUNDLE_CUSTODY_REPORT)
    {
        EV << "MESSAGE RECEIVED IN SAT" << endl;
        BundlePkt* bundle = check_and_cast<BundlePkt *>(msg);

        if (eid_ == bundle->getNextHopEid())
        {
            // This is an inbound message, check if packet was lost on the way
            if (packetLoss_ > uniform(0, 1.0))
            {
                // Packet was lost in the way, delete it
                cout << simTime() << " Node " << eid_ << " Bundle id " << bundle->getBundleId() << " lost on the way!" << endl;
                delete bundle;
            }
            else
            {
                // received correctly, send to Dtn layer
                send(msg, "dtnTransportOut");
            }
        }
        else
        {
            // TODO: needs to be rewritten to new dynamic gate system

            // This is an outbound message, perform a delayed send
            // if (bundle->getNextHopEid() > getSystemModule()->getSubmoduleVectorSize("groundStation")) {
            //     // Sat to Sat connection
            //     int nextSatId = bundle->getNextHopEid() - getSystemModule()->getSubmoduleVectorSize("groundStation") - 1;
            //     isldirection::ISLDirection  nextSatDirection = getSatISLDirection(this->getParentModule()->getIndex(), nextSatId);
            //     cGate* gate = getGate(nextSatDirection);
            //     double linkDelay = contactTopology_->getRangeBySrcDst(eid_, bundle->getNextHopEid());
            //     if (linkDelay == -1)
            //     {
            //         cout << "warning, range not available for nodes " << eid_ << "-" << bundle->getNextHopEid() << ", assuming range is 0" << endl;
            //         linkDelay = 0;
            //     }
            //     sendDelayed(bundle, linkDelay, gate);
            // } else {
            //     EV << "MESSAGE TO GROUNDLINLK" << endl;
            //     int gateIndex = topologyControl->getGroundstationSatConnection(bundle->getNextHopEid() - 1, this->getParentModule()->getIndex()).satGateIndex;
            //     double linkDelay = contactTopology_->getRangeBySrcDst(eid_, bundle->getNextHopEid());
            //     if (linkDelay == -1)
            //     {
            //         cout << "warning, range not available for nodes " << eid_ << "-" << bundle->getNextHopEid() << ", assuming range is 0" << endl;
            //         linkDelay = 0;
            //     }

            //     sendDelayed(bundle, linkDelay, "groundLink$o", gateIndex);
            // }
        }
    }
}

}  // namespace florasat
