/*
 * DtnSatellitePacketHandler.h
 *
 * Created on: Jun 16, 2023
 *     Author: Sebastian Montoya
 * Updated on: Jan 08, 2024
 *     Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>
#include <stdio.h>
#include <string.h>

#include <fstream>
#include <iomanip>

#include "inet/common/INETDefs.h"
#include "inet/common/Simsignals.h"

#include "core/mobility/INorad.h"
#include "core/mobility/NoradA.h"
#include "orbit/constellation/topologycontrol/dtn/DtnTopologyControl.h"
#include "networklayer/RoutingBase.h"
#include "dtn/DtnRoutingHeader_m.h"
#include "dtn/MsgTypes.h"
#include "dtn/contactplan/ContactPlan.h"

using namespace omnetpp;

namespace florasat {

class DtnSatellitePacketHandler : public cSimpleModule {
   protected:
    int satIndex = -1;
    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void handleMessage(cMessage* msg) override;

   private:
    int eid_;
    ContactPlan* contactTopology_;
    DtnTopologyControl* topology_control;
    double packetLoss_;
};

}  // namespace florasat
