#ifndef __FLORA_ROUTING_DTN_COM_H_
#define __FLORA_ROUTING_DTN_COM_H_

#include "dtn/contactplan/ContactPlan.h"
#include "orbit/constellation/topologycontrol/TopologyControlBase.h"

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include <fstream>
#include <iomanip>

#include "dtn/MsgTypes.h"
#include "dtn/DtnRoutingHeader_m.h"

namespace florasat {
namespace routing {

using namespace std;
using namespace omnetpp;

class Com: public cSimpleModule
{
protected:
    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void handleMessage(cMessage *msg) override;

private:

	int eid_;
	ContactPlan* contactTopology_;
	TopologyControlBase* topologyControl;
	double packetLoss_;

};

} // namespace florasat
} // namespace routing
#endif /* __FLORA_ROUTING_DTN_COM_H_ */
