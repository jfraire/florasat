/*
 * DtnGroundStationRouting.h
 *
 *  Created on: May 30, 2023
 *      Author: Sebastian Montoya
 */

#ifndef __FLORA_GROUND_DTNGROUNDSTATIONROUTING_H_
#define __FLORA_GROUND_DTNGROUNDSTATIONROUTING_H_

#include "ground/GroundStationRoutingBase.h"

using namespace omnetpp;
using namespace florasat::core;

namespace florasat {
namespace ground {

class DtnGroundStationRouting : public GroundStationRoutingBase {
  protected:
    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
};

}  // namespace ground
}  // namespace florasat

#endif  // __FLORA_GROUND_DTNGROUNDSTATIONROUTING_H_
