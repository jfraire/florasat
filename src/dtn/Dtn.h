#ifndef _FLORA_ROUTING_DTN_H_
#define _FLORA_ROUTING_DTN_H_

#include <cstdio>
#include <string>
#include <omnetpp.h>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <queue>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "dtn/contactplan/ContactPlan.h"
#include "dtn/ContactHistory.h"
#include "dtn/CustodyModel.h"
#include "dtn/Routing.h"
#include "dtn/RoutingCgrModelRev17.h"
#include "dtn/SdrModel.h"
#include "dtn/utils/MetricCollector.h"
#include "dtn/MsgTypes.h"
#include "dtn/DtnRoutingHeader_m.h"
#include "dtn/Routing.h"


using namespace omnetpp;
using namespace std;

namespace florasat {
namespace routing {

class Dtn: public cSimpleModule, public Observer
{
public:
    virtual void setOnFault(bool onFault);
    virtual void refreshForwarding();
    ContactPlan * getContactPlanPointer();
    virtual void setMetricCollector(MetricCollector* metricCollector);
    virtual Routing * getRouting();
    virtual void update(void) override;
    void setShiftedEid();

    //Opportunistic procedures
    void syncDiscoveredContact(Contact* c, bool start);
    void syncDiscoveredContactFromNeighbor(Contact* c, bool start, int ownEid, int neighborEid);
    void scheduleDiscoveredContactStart(Contact* c);
    void scheduleDiscoveredContactEnd(Contact* c);
    ContactHistory* getContactHistory();
    void addDiscoveredContact(Contact c);
    void removeDiscoveredContact(Contact c);
    void predictAllContacts(double currentTime);
    void coordinateContactStart(Contact* c);
    void coordinateContactEnd(Contact* c);
    void notifyNeighborsAboutDiscoveredContact(Contact* c, bool start, map<int,int>* alreadyInformed);
    void updateDiscoveredContacts(Contact* c);
    map<int,int> getReachableNodes();
    void addCurrentNeighbor(int neighborEid);
    void removeCurrentNeighbor(int neighborEid);
    int checkExistenceOfContact(int sourceEid, int destinationEid, int start);


protected:
    virtual void initialize(int stage) override;
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void handleMessage(cMessage *msg) override;
    virtual void finish() override;
    virtual void dispatchBundle(BundlePkt *bundle);

private:
    int eid_;
    bool onFault = false;

    // Forwarding threads
    map<int, ForwardingMsgStart *> forwardingMsgs_;

    // Routing and storage
    Routing * routing;

    // Contact Plan to feed CGR
    // and get transmission rates
    ContactPlan* contactPlan_;

    // Contact History used to collect all
    // discovered contacts;
    ContactHistory contactHistory_;

    //An observer that collects and evaluates all the necessary simulation metrics
    MetricCollector* metricCollector_;

    // Contact Topology to schedule Contacts
    // and get transmission rates
    ContactPlan* contactTopology_;

    CustodyModel custodyModel_;
    double custodyTimeout_;

    SdrModel sdr_;

    // BundlesMap
    bool saveBundleMap_;
    ofstream bundleMap_;

    // Signals
    simsignal_t dtnBundleSentToCom;
    simsignal_t dtnBundleSentToApp;
    simsignal_t dtnBundleSentToAppHopCount;
    simsignal_t dtnBundleSentToAppRevisitedHops;
    simsignal_t dtnBundleReceivedFromCom;
    simsignal_t dtnBundleReceivedFromApp;
    simsignal_t dtnBundleReceivedFromPacketGenerator;
    simsignal_t dtnBundleReRouted;
    simsignal_t sdrBundleStored;
    simsignal_t sdrBytesStored;
    simsignal_t routeCgrDijkstraCalls;
    simsignal_t routeCgrDijkstraLoops;
    simsignal_t routeCgrRouteTableEntriesCreated;
    simsignal_t routeCgrRouteTableEntriesExplored;
};

}  // namespace routing
}  // namespace florasat

#endif /* _FLORA_ROUTING_DTN_H_ */


