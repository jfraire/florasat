/*
 * PacketGenerator.h
 *
 *  Created on: May 27, 2023
 *      Author: Sebastian Montoya
 */

#ifndef __FLORA_GROUND_DTNPACKETGENERATOR_H_
#define __FLORA_GROUND_DTNPACKETGENERATOR_H_

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

#include "inet/common/INETDefs.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/Simsignals.h"
#include "inet/common/packet/Message.h"
#include "inet/common/packet/Packet.h"

#include "networklayer/ConstellationRoutingTable.h"
#include "networklayer/RoutingBase.h"
#include "orbit/constellation/topologycontrol/TopologyControlBase.h"
#include "dtn/MsgTypes.h"
#include "dtn/DtnRoutingHeader_m.h"
#include "dtn/DtnTransportHeader_m.h"
#include "dtn/RoutingCgrModelRev17.h"
#include "dtn/CustodyModel.h"
#include "ground/TransportHeader_m.h"


using namespace omnetpp;
using namespace inet;

namespace florasat {

class DtnPacketGenerator : public cSimpleModule {
    public:
        virtual std::vector<int> getBundlesNumberVec();
        virtual std::vector<int> getDestinationEidVec();
        virtual std::vector<int> getSizeVec();
        virtual std::vector<double> getStartVec();
    protected:
        TopologyControlBase *topologycontrol;
        networklayer::ConstellationRoutingTable *routingTable;
        ContactPlan *contactPlan;

    protected:
        virtual void initialize(int stage) override;
        virtual void finish() override;
        virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }

        int sentPackets;
        int groundStationId;
        int numGroundStations;
        int numSatellites;

    protected:
        virtual void handleMessage(cMessage *msg) override;

    private:
        std::vector<int> bundlesNumber;
        std::vector<int> destinationsEid;
        std::vector<int> sizes;
        std::vector<double> starts;
        void parseBundlesNumber();
        void parseDestinationsEid();
        void parseSizes();
        void parseStarts();
        int eid_;
        // Signal
        simsignal_t appBundleSent;
        simsignal_t appBundleReceived;
        simsignal_t appBundleReceivedHops;
        simsignal_t appBundleReceivedDelay;
};

}  // namespace florasat

#endif  // __FLORA_GROUND_DTNPACKETGENERATOR_H_
