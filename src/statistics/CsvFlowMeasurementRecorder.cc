/*
 * CsvFlowMeasurementRecorder.cc
 *
 *  Created on: Jan 08, 2024
 *      Author: Robin Ohs
 */

#include "CsvFlowMeasurementRecorder.h"

namespace florasat {

Define_Module(CsvFlowMeasurementRecorder);

/** Class */

CsvFlowMeasurementRecorder::~CsvFlowMeasurementRecorder() {
    file.close();
}

/////////////////////////////////////
/** OMNETPP */

static bool matchesString(cMatchExpression& matchExpression, const char* string) {
    cMatchableString matchableString(string);
    return matchExpression.matches(&matchableString);
}

void CsvFlowMeasurementRecorder::initialize(int stage) {
    FlowMeasurementRecorder::initialize(stage);
    if (stage == INITSTAGE_LOCAL) {
        auto f_name = par("file_name").stdstringValue();
        std::string file_name = f_name + ".csv";

        // write header to file
        std::ofstream file_tmp;
        file_tmp.open(file_name);
        file_tmp << "created_us,recorded_us,src_gs,dst_gs,num_hops,size_bits, prop_delay_us, trans_delay_us, queueing_delay_us, proc_delay_us, drop_reason";
        file_tmp << "\n";
        file_tmp.close();

        // open file for appending
        file.open(file_name, std::ios_base::app);

        // check for matches
        cMatchExpression measureMatcher;
        measureMatcher.setPattern(par("measure"), false, true, true);
        measureHops = matchesString(measureMatcher, "hops");
        measureStations = matchesString(measureMatcher, "stations");
        measureCreationTime = matchesString(measureMatcher, "creationTime");
    }
}

/////////////////////////////////////
/** IMPLEMENTATION */

void CsvFlowMeasurementRecorder::makeMeasurements(Packet* packet) {
    FlowMeasurementRecorder::makeMeasurements(packet);
    EV << "Record packet " << packet << endl;

    // write statistics
    // times
    if (measureCreationTime) {
        file << packet->getTag<CreationTimeTag>()->getCreationTime().inUnit(SimTimeUnit::SIMTIME_US);
    } else {
        file << -1;
    }
    file << ", " << simTime().inUnit(SimTimeUnit::SIMTIME_US);
    // src/dst
    if (measureStations) {
        auto metadata_tag = packet->getTag<MetadataTag>();
        size_t src_gs_id = metadata_tag->getSrcGs();
        size_t dst_gs_id = metadata_tag->getDstGs();
        file << ", " << src_gs_id;
        file << ", " << dst_gs_id;
    } else {
        file << ", " << -1;
        file << ", " << -1;
    }
    // hops
    if (measureHops) {
        auto metadata_tag = packet->getTag<MetadataTag>();
        size_t hops = metadata_tag->getHops();
        file << ", " << hops;
    } else {
        file << ", " << -1;
    }
    // size
    file << ", " << packet->getTotalLength().get();
    // delays
    // propagation time
    if (measurePropagationTime) {
        auto tags = packet->getAllRegionTags<PropagationTimeTag>();
        ASSERT(tags.size() == 1);
        auto tag = tags[0].getTag().get();
        auto time = get_total_time(tag);
        file << ", " << time.inUnit(SimTimeUnit::SIMTIME_US);
    } else {
        file << ", -1";
    }
    // transmission time
    if (measureTransmissionTime) {
        auto tags = packet->getAllRegionTags<TransmissionTimeTag>();
        ASSERT(tags.size() == 1);
        auto tag = tags[0].getTag().get();
        auto time = get_total_time(tag);
        file << ", " << time.inUnit(SimTimeUnit::SIMTIME_US);
    } else {
        file << ", -1";
    }
    // queueing time
    if (measureQueueingTime) {
        auto tags = packet->getAllRegionTags<QueueingTimeTag>();
        ASSERT(tags.size() == 1);
        auto tag = tags[0].getTag().get();
        auto time = get_total_time(tag);
        file << ", " << time.inUnit(SimTimeUnit::SIMTIME_US);
    } else {
        file << ", -1";
    }
    // processing time
    if (measureProcessingTime) {
        auto tags = packet->getAllRegionTags<ProcessingTimeTag>();
        ASSERT(tags.size() == 1);
        auto tag = tags[0].getTag().get();
        auto time = get_total_time(tag);
        file << ", " << time.inUnit(SimTimeUnit::SIMTIME_US);
    } else {
        file << ", -1";
    }
    // drop reason
    file << ", -1";
    file << "\n";
}

simtime_t CsvFlowMeasurementRecorder::get_total_time(const inet::TimeTagBase* time_tag) {
    int flows = time_tag->getPacketTotalTimesArraySize();
    if (flows > 0) {
        return time_tag->getPacketTotalTimes(flows - 1);
    } else {
        return SimTime::ZERO;
    }
}

}  // namespace florasat