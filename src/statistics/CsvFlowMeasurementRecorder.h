/*
 * CsvFlowMeasurementRecorder.h
 *
 *  Created on: Jan 08, 2024
 *      Author: Robin Ohs
 */

#pragma once

#include <omnetpp.h>

#include <fstream>
#include <optional>
#include <string>

#include "MetadataTag_m.h"
#include "inet/common/INETUtils.h"
#include "inet/common/TimeTag.h"
#include "inet/common/packet/Packet.h"
#include "inet/queueing/flow/FlowMeasurementRecorder.h"

using namespace omnetpp;
using namespace inet;
using namespace inet::queueing;

namespace florasat {

class CsvFlowMeasurementRecorder : public FlowMeasurementRecorder {
   protected:
    virtual ~CsvFlowMeasurementRecorder();
    virtual void makeMeasurements(Packet *packet) override;

    virtual void initialize(int stage) override;
    simtime_t get_total_time(const inet::TimeTagBase *time_tag);

   protected:
    bool measureCreationTime = false;
    bool measureStations = false;
    bool measureHops = false;

    std::ofstream file;
};

}  // namespace florasat